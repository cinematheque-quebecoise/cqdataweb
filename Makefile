BLZG_JNL_PATH=$(TMPDIR)/blazegraph.jnl
BLZG_PORT=9998
JAVA_XMX=8g

run-dev: app/node_modules $(BLZG_JNL_PATH)
	cd scripts && ./run-dev $(BLAZEGRAPH_JAR_FPATH) $(BLZG_PORT) $(BLZG_JNL_PATH) $(JAVA_XMX)

app/node_modules: app/package.json
	cd app && npm i

$(BLZG_JNL_PATH):
	loadDataBlazegraph $(BLAZEGRAPH_JAR_FPATH) $(BLZG_PORT) $(BLZG_JNL_PATH) $(JAVA_XMX) http://localhost:8080

vbox-init:
	nixops create nixops/cqdataweb.nix nixops/cqdataweb-vbox.nix -d cqdataweb-vbox -s vbox.nixops

vbox-stop:
	nixops stop -d cqdataweb-vbox -s vbox.nixops

vbox-destroy:
	nixops destroy -d cqdataweb-vbox -s vbox.nixops
	nixops delete -d cqdataweb-vbox -s vbox.nixops

vbox-deploy: app/default.nix app/node-env.nix app/node-packages.nix
	nixops deploy -d cqdataweb-vbox -s vbox.nixops --force-reboot

vbox-ssh:
	nixops ssh -s vbox.nixops -d cqdataweb-vbox cqdataweb

digitalocean-init:
	nixops create nixops/cqdataweb.nix nixops/cqdataweb-docean.nix -d cqdataweb-docean -s docean.nixops

digitalocean-import:
	nixops import -s docean.nixops < nixops/cqdataweb.json
	nixops modify -s docean.nixops -d cqdataweb-docean nixops/cqdataweb.nix nixops/cqdataweb-docean.nix

digitalocean-export:
	nixops export -d cqdataweb-docean > nixops/cqdataweb.json

digitalocean-deploy: app/default.nix app/node-env.nix app/node-packages.nix
	nixops deploy -d cqdataweb-docean -s docean.nixops

digitalocean-ssh:
	nixops ssh -s docean.nixops -d cqdataweb-docean cqdataweb

app/default.nix: app/package-lock.json
	cd app && node2nix -l package-lock.json
app/node-env.nix: app/package-lock.json
	cd app && node2nix -l package-lock.json
app/node-packages.nix: app/package-lock.json
	cd app && node2nix -l package-lock.json

clean:
	rm -rf result* app/node_modules *.jnl *_temp *.venv logs *.nixops *.log
