{ pkgs }:

let
appareils-assets-version = "0.1";

appareils-assets-json = builtins.fromJSON (builtins.readFile (pkgs.fetchurl {
  url = "https://gitlab.com/api/v4/projects/25083708/releases/v${appareils-assets-version}/assets/links";
  sha256 = "e58bae13774b153131fcd939bc3852107a49057c98ba65e52a07e0ba28d5640a";
}));
appareils-images-url = (builtins.head (builtins.filter (v: v.name == "NUM_appareils.zip") appareils-assets-json)).direct_asset_url;

in pkgs.fetchzip {
  url = "${appareils-images-url}";
  sha256 = "1sb55mgkx4ry86r8v64f7xcdxph82riysql45gzd5izx2icf2bc0";
}
