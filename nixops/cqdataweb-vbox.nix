{
  resources.sshKeyPairs.ssh-key = {};

  cqdataweb = { pkgs, ... }: {
    services.openssh.enable = true;

    deployment.targetEnv = "virtualbox";
    deployment.virtualbox.memorySize = 2048; # megabytes
    deployment.virtualbox.vcpu = 1; # number of cpus
  };
}

