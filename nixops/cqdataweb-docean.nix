{
  resources.sshKeyPairs.ssh-key = {};

  cqdataweb = { pkgs, ... }: {
    services.openssh.enable = true;

    deployment.targetEnv = "digitalOcean";
    deployment.digitalOcean.enableIpv6 = false;
    deployment.digitalOcean.region = "nyc3";
    deployment.digitalOcean.size = "s-1vcpu-2gb";
  };
}
