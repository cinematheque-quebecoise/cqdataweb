{
  network.description = "CQ data web server";

  cqdataweb =
    { config, pkgs, ... }:
    let
      app = (import ../app/override.nix {}).package;

      waitserver = import ../scripts/waitserver.nix { inherit pkgs; };

      loadDataBlazegraph = import ../scripts/loadDataBlazegraph.nix { inherit pkgs; };

      runBlazegraph = import ../scripts/runBlazegraph.nix { inherit pkgs; };

      rdfBaseUri = "http://data.cinematheque.qc.ca";

      blazegraphJar = import ../nix/blazegraph.nix { inherit pkgs; };

      blazegraphJnlPath = "/var/lib/blazegraph/blazegraph.jnl";

      blazegraphPort = "9998";

      cmtqServerPort = "3000";

      javaXmx = "3g";

      appareilsImagesPath = import ../nix/appareils.nix { inherit pkgs; };
    in
    {
      networking.firewall.allowedTCPPorts = [ 80 443 ];
      services.nginx = {
        enable = true;
        # virtualHosts."default" = {
        virtualHosts."data.cinematheque.qc.ca" = {
          enableACME = true;
          forceSSL = true;
          locations = {
            "/" = {
              proxyPass = "http://localhost:${cmtqServerPort}";
              extraConfig = "proxy_set_header Host $host;";
            };
            "/sparql" = {
              proxyPass = "http://localhost:${blazegraphPort}/blazegraph/sparql";
              extraConfig = "add_header \"Access-Control-Allow-Origin\" \"*\";";
            };
            "/dataviz/polyscc" = {
              proxyPass = "http://159.89.115.31/dataviz/polyscc";
            };
          };
        };
      };
      security.acme = {
        acceptTerms = true;
        certs = {
          "data.cinematheque.qc.ca".email = "klambroulatreille@cinematheque.qc.ca";
        };
      };
      systemd.services.cqdataweb = {
        enable = true;
        serviceConfig = {
          WorkingDirectory = "${app}/lib/node_modules/cqdataweb";
          ExecStart = "${pkgs.nodejs-10_x}/bin/node app.js --baseuri ${rdfBaseUri} --port ${cmtqServerPort}";
          Environment = "APPAREILS_IMAGES_PATH=${appareilsImagesPath}";
        };
        wantedBy = [ "multi-user.target" ];
      };
      systemd.services.blazegraph = {
        enable = true;
        serviceConfig = {
          ExecStartPre = "${loadDataBlazegraph}/bin/loadDataBlazegraph ${blazegraphJar} ${blazegraphPort} ${blazegraphJnlPath} ${javaXmx} ${rdfBaseUri}";
          ExecStart = "${runBlazegraph}/bin/runBlazegraph ${blazegraphJar} ${blazegraphPort} ${blazegraphJnlPath} ${javaXmx}";
          # ExecStart = "${pkgs.openjdk}/bin/java -server -Xmx2g -Djetty.port=${blazegraphPort} -Djetty.overrideWebXml=${webXml} -Dcom.bigdata.rdf.sail.webapp.ConfigParams.propertyFile=${nssProperties} -Dcom.bigdata.journal.AbstractJournal.file=/var/lib/blazegraph/blazegraph.jnl -jar ${blazegraphJar}";
          # ExecStartPost = "+${loadRdfData}/bin/loadRdfData ${rdfBaseUri}/ontology/cmtqo ${rdfBaseUri} ${nssProperties}";
          TimeoutSec=1800;
          PermissionsStartOnly=true;
          Environment = "TMPDIR=/tmp";
        };
        wantedBy = [ "multi-user.target" ];
      };
    };
}
