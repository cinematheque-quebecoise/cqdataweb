{
  "nav": {
    "home": "Accueil",
    "ontology": "Ontologie",
    "spec": "Spécification",
    "graphol": "Diagramme Graphol",
    "contribution": "Contribution",
    "lod": "Données ouvertes et liées",
    "lod-schema": "Schéma des données",
    "lod-dump": "Jeux de données",
    "lod-sparql": "Éditeur SPARQL",
    "dataviz": "Visualisations de données",
    "od": "Données ouvertes",
    "realisations": "Réalisations",
    "visualisations": "Visualisations de données",
    "licence": "Licence"
  },
  "home": {
    "welcome": "Bienvenue",
    "welcome-content": "Bienvenue dans le site web de diffusion des données de la Cinémathèque québécoise. Ce site web a été initié dans le cadre de l'initiative Savoirs Communs du Cinéma de la Cinémathèque québécoise. L'objectif de ce site est d'offrir aux gens la possibilité d'utiliser nos données à l'aide des technologies du Web Sémantique (RDF, SPARQL, OWL)."
  },
  "ontology": {
    "contribution": {
      "title": "Contributions",
      "text": "Pour contribuer à la construction de l'ontologie, allez vers le lien suivant: <a href=\"#\">https://gitlab.com/cinematheque-qc/ontology</a>"
    }
  },
  "licence": {
    "text": "Toutes les données publiées sur le site Web <a href=\"\">data.cinematheque.qc.ca</a> sont disponibles sous la licence <a href=\"\">CC-BY-SA</a>. Vous êtes libre d'utiliser, de partager et d'adapter le contenu <a href=\"\">CC-BY-SA</a> tout en respectant les conditions d'attribution et de partage sous les mêmes conditions.",
    "attribution": "L'attribution devrait mentionner la <a href=\"https://www.cinematheque.qc.ca\">Cinémathèque québécoise</a>."
  }
}
