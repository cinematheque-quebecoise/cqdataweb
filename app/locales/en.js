{
  "nav": {
    "home": "Home",
    "ontology": "Ontologie",
    "spec": "Specification",
    "graphol": "Graphol diagram",
    "contribution": "Contribution",
    "lod": "Open linked data",
    "od": "Other data",
    "realisations": "Realisations",
    "licence": "Licence"
  }
}
