const i18n = require('i18n')
const path = require('path')

i18n.configure({
  locales: ['fr'],
  defaultLocale: 'fr',
  queryParameter: 'lang',
  directory: path.join(__dirname, 'locales'),
  autoReload: true,
  updateFiles: false,
  indent: "  ",
  extension: '.js',
  objectNotation: true,
  api: {
    '__': 't',
    '__n': 'tn'
  },
});

module.exports = i18n
