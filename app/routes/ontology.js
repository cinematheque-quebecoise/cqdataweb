const axios = require('axios')
const { getLatestGitlabProjectReleaseVersion } = require('./util');

const CMTQO_GITLAB_PROJECT_ID = '18249681'

module.exports = baseuri => router => {
  router.get('/ontology/cmtqo/wiki', async (ctx, next) => {
    await ctx.render('cmtqowiki')
  })

  router.get('/ontology/cmtqo', async (ctx, next) => {
    try {
      ctx.state.version = await getLatestGitlabProjectReleaseVersion(CMTQO_GITLAB_PROJECT_ID)
      await next()
    } catch {
      return
    }
  }, sendOntologyDoc)

  router.get('/ontology/cmtqo/:version', async (ctx, next) => {
    ctx.state.version = `v${ctx.params.version}`
    await next()
  }, sendOntologyDoc)

  async function sendOntologyDoc (ctx, next) {
    try {
      const version = ctx.state.version
      const assetsResult = await axios.get(`https://gitlab.com/api/v4/projects/${CMTQO_GITLAB_PROJECT_ID}/releases/${version}/assets/links`)
      const fileAsset = assetsResult.data.find(a => a.name === `cmtqo.md`)
      const fileContentResult = await axios.get(fileAsset.url)
      const content = fileContentResult.data
      await ctx.render('specification', { content })
    } catch {
      await next()
    }
  }

  router.get('/ontology/cmtqo/:version(.*):ext(\.ttl)', async (ctx, next) => {
    ctx.state.version = `v${ctx.params.version}`
    ctx.state.ext = ctx.params.ext
    await next()
  }, sendOntologyFileMiddleware)

  router.get('/ontology/cmtqo:ext(\.ttl)', async (ctx, next) => {
    ctx.state.version = await getLatestGitlabProjectReleaseVersion(CMTQO_GITLAB_PROJECT_ID)
    ctx.state.ext = ctx.params.ext

    await next()
  }, sendOntologyFileMiddleware)

  router.get('/doc/ontology/cmtqo/graphol', async (ctx, next) => {
    try {
      ctx.state.version = await getLatestGitlabProjectReleaseVersion(CMTQO_GITLAB_PROJECT_ID)
    } catch {
      return
    }

    await next()

  }, sendGrapholPageRes)

  router.get('/doc/ontology/cmtqo/:version/graphol', async (ctx, next) => {
    ctx.state.version = `v${ctx.params.version}`
    await next()
  }, sendGrapholPageRes)

  async function sendGrapholPageRes (ctx, next) {
    const version = ctx.state.version

    try {
      const assetsResult = await axios.get(`https://gitlab.com/api/v4/projects/${CMTQO_GITLAB_PROJECT_ID}/releases/${version}/assets/links`)
      console.log(assetsResult.data)
      await ctx.render('graphol', {
        assets: assetsResult.data
      })
    } catch {
      await next()
    }
  }

  async function sendOntologyFileMiddleware (ctx, next) {
    const version = ctx.state.version
    const ext = ctx.state.ext

    try {
      const assetsResult = await axios.get(`https://gitlab.com/api/v4/projects/${CMTQO_GITLAB_PROJECT_ID}/releases/${version}/assets/links`)
      const fileAsset = assetsResult.data.find(a => a.name === `cmtqo${ext}`)
      if (fileAsset === undefined) {
        throw new Error(`Can't find ontology file cmtqo${ext}`)
      }
      const res = await axios({
        method: 'get',
        url: fileAsset.url,
        responseType: 'stream'
      })
      ctx.body = res.data
    } catch {
      return
    }
  }
}
