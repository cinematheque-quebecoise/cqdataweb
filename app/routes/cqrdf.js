const axios = require('axios')
const YAML = require('yaml')
const path = require('path')
const prettyBytes = require('pretty-bytes');

const moment = require('moment')
moment.locale('fr-ca')

const gitlab = require('./gitlab')

const DATASET_GITLAB_PROJECT_ID = '18031890'

const getDatasetFilesPerDate = async () => {
  const datasets = await gitlab.fetchGitlabAssets(DATASET_GITLAB_PROJECT_ID)
  return datasets.map(d => {
    return {
      ...d,
      files: d.files.filter(f => f.filename.startsWith('cmtq') || f.filename.startsWith('void'))
    }
  })
}

const getSparqlQueryExamples = async () => {
  const releasesResult = await axios.get(`https://gitlab.com/api/v4/projects/${DATASET_GITLAB_PROJECT_ID}/releases`)
  const latestTagName = releasesResult.data.map(t => t.tag_name).sort((t1, t2) => t1 < t2 ? 1 : t1 > t2 ? -1 : 0)[0]

  const assetsResult = await axios.get(`https://gitlab.com/api/v4/projects/${DATASET_GITLAB_PROJECT_ID}/releases/${latestTagName}/assets/links`)
  const exampleQueriesAsset = assetsResult.data.find(a => a.name === 'example-queries.yaml')

  const sparqlQueriesRes = await axios.get(exampleQueriesAsset.url)
  const sparqlQueriesStr = sparqlQueriesRes.data

  return YAML.parse(sparqlQueriesStr)
}

module.exports = {
  getDatasetFilesPerDate,
  getSparqlQueryExamples
}

