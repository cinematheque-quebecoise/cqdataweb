const ontologyRoutes = require('./ontology')
const datasetsRoutes = require('./datasets')
const sparqlRoutes = require('./sparql')
const voidRoutes = require('./void')

module.exports = baseuri => router => {

  router.get('/', async (ctx, next) => {
    await ctx.render('home')
  })

  router.get('/doc/dataviz', async (ctx, next) => {
    await ctx.render('dataviz')
  })

  router.get('/doc/licence', async (ctx, next) => {
    await ctx.render('licence')
  })

  router.get('/about/javascript', async (ctx, next) => {
    await ctx.render('about')
  })

  // ontologyRoutes(baseuri)(router)

  datasetsRoutes(baseuri)(router)

  sparqlRoutes(baseuri)(router)

  voidRoutes(baseuri)(router)
}
