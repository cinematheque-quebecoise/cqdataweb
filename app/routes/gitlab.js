const axios = require('axios')
const path = require('path')
const prettyBytes = require('pretty-bytes');

const moment = require('moment')
moment.locale('fr-ca')

const DATASET_GITLAB_PROJECT_ID = "25083708"

const fetchGitlabAssets = async (projectId) => {
  const releasesResult = await axios.get(`https://gitlab.com/api/v4/projects/${projectId}/releases`)
  const datasets = []
  await Promise.all(releasesResult.data
    .sort((t1, t2) => t1.tag_name < t2.tag_name ? 1 : t1.tag_name > t2.tag_name ? -1 : 0)
    .map(async release => {
      const releasedAt = new Date(release.released_at)
      const releaseDate = moment(releasedAt).format('L')

      const assetsResult = await axios.get(`https://gitlab.com/api/v4/projects/${projectId}/releases/${release.tag_name}/assets/links`)

      const datasetFiles = await Promise.all(assetsResult.data
        .map(async a => {
          const fileResult = await axios.head(a.url)
          const filesize = parseInt(fileResult.headers['content-length'])
          return {
            ext: path.extname(a.name.replace('.gz', '')),
            filename: a.name,
            filesize: isNaN(filesize) ? undefined : prettyBytes(filesize),
            url: a.url
          }
        }))
      datasets.push({
        date: releaseDate,
        files: datasetFiles
      })
    }))
  return datasets
}

module.exports = {
  fetchGitlabAssets
}
