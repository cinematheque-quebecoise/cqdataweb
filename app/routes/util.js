const axios = require('axios')

module.exports = {
  getLatestGitlabProjectReleaseVersion: async (projectId) => {
    const releasesResult = await axios.get(`https://gitlab.com/api/v4/projects/${projectId}/releases`)
    const tagNames = releasesResult.data
      .map(release => release.tag_name)
      .sort((t1, t2) => t1 < t2 ? 1 : t1 > t2 ? -1 : 0)
    if (tagNames.length > 0) {
      return tagNames[0]
    }
  }
}
