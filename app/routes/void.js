const axios = require('axios')
const { getLatestGitlabProjectReleaseVersion } = require('./util');

const CMTQO_GITLAB_PROJECT_ID = '18249681'

module.exports = baseuri => router => {
  router.get('/.well-known/void', async (ctx, next) => {
    ctx.set({
      'Location': `/void/Dataset`
    })
    ctx.status = 302
  })
}
