const url = require('url')
const cqrdf = require('./cqrdf')

const exampleQueriesFactory = baseUri => {
  return [
    {
      name: 'Réalisateurs de films où Michel Côté a joué.',
      query: `PREFIX cmtqo: <http://data.cinematheque.qc.ca/ontology/cmtqo/0.1#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX frbroo: <http://iflastandards.info/ns/fr/frbr/frbroo/>
PREFIX crm: <http://www.cidoc-crm.org/cidoc-crm/>
PREFIX cmtq: <${baseUri}>

SELECT DISTINCT ?director ?directorLabel WHERE {
?productionEvent frbroo:R22_created_a_realization_of ?filmo .
?productionEvent crm:P9_consists_of ?michelCoteRole .
?michelCoteRole crm:P14_carried_out_by cmtq:Person56042 .

?productionEvent crm:P9_consists_of ?directorRole .
?directorRole cmtqo:in_the_role_of cmtq:Role1 .
?directorRole crm:P14_carried_out_by ?director .
?director rdfs:label ?directorLabel .
} limit 100`
    },
    // {
    //   name: 'Tous les films qui sont inspirés d'une autre oeuvre.',
    //   query: `SELECT * WHERE {
// ?s ?p ?o
// } LIMIT 100`
    // },
    {
      name: 'Tous les films avec une date de sortie entre 2017 et 2020.',
      query: `PREFIX crm: <http://www.cidoc-crm.org/cidoc-crm/>
PREFIX frbroo: <http://iflastandards.info/ns/fr/frbr/frbroo/>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX cmtqo: <http://data.cinematheque.qc.ca/ontology/cmtqo/0.1#>
PREFIX cmtq: <${baseUri}>

SELECT DISTINCT ?filmo ?filmoLabel ?year WHERE {
?filmo a frbroo:F21_Recording_Work .
?filmo rdfs:label ?filmoLabel .
?filmo cmtqo:release_event ?filmoReleaseEvent .
?filmoReleaseEvent crm:P4_has_time-span ?filmoReleaseTimeSpan .
optional {
  ?filmoReleaseTime crm:P79_beginning_is_qualified_by ?filmoReleaseBegin .
}
optional {
  ?filmoReleaseTime crm:P80_end_is_qualified_by ?filmoReleaseEnd .
}

BIND(year(?filmoReleaseBegin) as ?year)
BIND(year(?filmoReleaseEnd) as ?year)

filter (?year >= 2017 && ?year < 2020)
}
LIMIT 100`
    },
    {
      name: 'Les personnes qui ont travaillé dans au moins 10 films',
      query: `PREFIX cmtqo: <http://data.cinematheque.qc.ca/ontology/cmtqo/0.1#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX frbroo: <http://iflastandards.info/ns/fr/frbr/frbroo/>
PREFIX crm: <http://www.cidoc-crm.org/cidoc-crm/>
PREFIX cmtq: <${baseUri}>

SELECT DISTINCT ?person (COUNT(DISTINCT ?filmo) as ?filmoCount) WHERE {
?productionEvent frbroo:R22_created_a_realization_of ?filmo .
?productionEvent crm:P9_consists_of ?productionRoleActivity .
?productionRoleActivity crm:P14_carried_out_by ?person .
?person a crm:E21_Person
}
GROUP BY ?person
HAVING(COUNT(?filmo) > 10)
ORDER BY DESC (?filmoCount)
LIMIT 10`
    },
    {
      name: 'Évolution de la carrière de Woody Allen.',
      query: `PREFIX crm: <http://www.cidoc-crm.org/cidoc-crm/>
PREFIX frbroo: <http://iflastandards.info/ns/fr/frbr/frbroo/>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX cmtqo: <http://data.cinematheque.qc.ca/ontology/cmtqo/0.1#>
PREFIX cmtq: <${baseUri}>

SELECT DISTINCT ?filmo ?filmoLabel ?roleLabel ?year WHERE {
?filmo a frbroo:F21_Recording_Work .
?filmo rdfs:label ?filmoLabel .

?filmo cmtqo:release_event ?filmoReleaseEvent .
optional {
  ?filmoReleaseEvent crm:P4_has_time-span ?filmoReleaseTimeSpan .
  ?filmoReleaseTimeSpan crm:P79_beginning_is_qualified_by ?filmoReleaseBegin .
  ?filmoReleaseTimeSpan crm:P80_end_is_qualified_by ?filmoReleaseEnd .
}

?productionEvent frbroo:R22_created_a_realization_of ?filmo .
optional {
  ?productionEvent crm:P4_has_time-span ?productionEventTimeSpan .
  ?productionEventTimeSpan crm:P79_beginning_is_qualified_by ?productionEventBegin .
  ?productionEventTimeSpan crm:P80_end_is_qualified_by ?productionEventEnd .
}

BIND(year(?productionEventBegin) as ?year)
BIND(year(?productionEventEnd) as ?year)
BIND(year(?filmoReleaseBegin) as ?year)
BIND(year(?filmoReleaseEnd) as ?year)

?productionEvent crm:P9_consists_of ?productionRoleActivity .
?productionRoleActivity crm:P14_carried_out_by cmtq:Person19445 .
?productionRoleActivity cmtqo:in_the_role_of ?role .
?role rdfs:label ?roleLabel .
}
order by asc(?year)`
    },
    {
      name: 'Les films dans les données liés à une entité de Wikidata.',
      query: `PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX wdt: <http://www.wikidata.org/prop/direct/>
PREFIX frbroo: <http://iflastandards.info/ns/fr/frbr/frbroo/>
PREFIX cmtqo: <http://data.cinematheque.qc.ca/ontology/cmtqo/0.1#>
PREFIX cmtq: <${baseUri}>

SELECT DISTINCT ?filmo ?wikidataFilm ?filmoLabel WHERE {

?filmo a frbroo:F21_Recording_Work .
?filmo rdfs:label ?filmoLabel .
?filmo cmtqo:cmtq_id ?filmoCqId

BIND(xsd:string(?filmoCqId) as ?filmoCqIdStr)

SERVICE <https://query.wikidata.org/bigdata/namespace/wdq/sparql> {
  ?wikidataFilm wdt:P4276 ?filmoCqIdStr .
}
} LIMIT 50`
    },
    {
      name: 'La proportion d\'oeuvres cinémathographiques liées à une entité de Wikidata.',
      query: `PREFIX frbroo: <http://iflastandards.info/ns/fr/frbr/frbroo/>
PREFIX owl: <http://www.w3.org/2002/07/owl#>

SELECT ?filmoCount ?filmoLinkedWikidataCount (xsd:float(?filmoLinkedWikidataCount)/xsd:float(?filmoCount) as ?proportion) WHERE {
{
  SELECT (COUNT(DISTINCT ?filmo) as ?filmoCount) WHERE {
    ?filmo a frbroo:F21_Recording_Work .
  }
}
{
  SELECT (COUNT(DISTINCT ?filmoLinkedWikidata) as ?filmoLinkedWikidataCount) WHERE {
    ?filmoLinkedWikidata a frbroo:F21_Recording_Work .
    ?filmoLinkedWikidata owl:sameAs [] .
  }
}
}`
    },
    {
      name: 'Roles possibles dans le cinéma avec leur fréquence.',
      query: `PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX cmtqo: <http://data.cinematheque.qc.ca/ontology/cmtqo/0.1#>
PREFIX cmtq: <${baseUri}>

SELECT ?role ?roleLabel (count(?role) as ?count) WHERE {
[] cmtqo:in_the_role_of ?role .
?role rdfs:label ?roleLabel .
}
GROUP BY ?role ?roleLabel
ORDER BY DESC(?count)`
    },
    {
      name: 'Sujets de films possibles catalogués par la Cinémathèque québécoise avec leur fréquence.',
      query: `PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX cmtqo: <http://data.cinematheque.qc.ca/ontology/cmtqo/0.1#>

SELECT DISTINCT ?subject ?subjectLabel (count(?subject) as ?count) WHERE {
[] cmtqo:has_subject ?subject .
?subject rdfs:label ?subjectLabel
}
GROUP BY ?subject ?subjectLabel
ORDER BY DESC(?count)`
    },
    {
      name: 'Les personnes dans les données liés à une entité de Wikidata.',
      query: `PREFIX crm: <http://www.cidoc-crm.org/cidoc-crm/>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX owl: <http://www.w3.org/2002/07/owl#>

SELECT DISTINCT ?person ?personLabel ?personWikidata WHERE {
?person a crm:E21_Person .
?person rdfs:label ?personLabel .
?person owl:sameAs ?personWikidata
}
LIMIT 100`
    },
    {
      name: 'La proportion de personnes dans les données liées à une entité de Wikidata.',
      query: `PREFIX crm: <http://www.cidoc-crm.org/cidoc-crm/>
PREFIX owl: <http://www.w3.org/2002/07/owl#>

SELECT ?personCount ?personLinkedWikidataCount (xsd:float(?personLinkedWikidataCount)/xsd:float(?personCount) as ?proportion) WHERE {
{
  SELECT (COUNT(DISTINCT ?person) as ?personCount) WHERE {
    ?person a crm:E21_Person .
  }
}
{
  SELECT (COUNT(DISTINCT ?personLinkedWikidata) as ?personLinkedWikidataCount) WHERE {
    ?personLinkedWikidata a crm:E21_Person .
    ?personLinkedWikidata owl:sameAs [] .
  }
}
}`
    },
    {
      name: 'Le nombre d\'oeuvres cinématographiques réalisées par des hommes.',
      query: `PREFIX wdt: <http://www.wikidata.org/prop/direct/>
PREFIX wd: <http://www.wikidata.org/entity/>
PREFIX frbroo: <http://iflastandards.info/ns/fr/frbr/frbroo/>
PREFIX crm: <http://www.cidoc-crm.org/cidoc-crm/>
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX cmtqo: <http://data.cinematheque.qc.ca/ontology/cmtqo/0.1#>
PREFIX cmtq: <${baseUri}>

SELECT DISTINCT (COUNT(DISTINCT ?workDirectedByMale) as ?workDirectedByMaleCount) WHERE {
?workDirectedByMale a frbroo:F21_Recording_Work .
?recordingEvent frbroo:R22_created_a_realization_of ?workDirectedByMale .
?recordingEvent crm:P9_consists_of ?roleActivity .
?roleActivity cmtqo:in_the_role_of cmtq:Role1 .
?roleActivity crm:P14_carried_out_by ?director .
?director owl:sameAs ?directorLinkWikidata .
SERVICE <https://query.wikidata.org/bigdata/namespace/wdq/sparql> {
  ?directorLinkWikidata wdt:P21 wd:Q6581097 .
}
}`
    },
    {
      name: 'Le nombre d\'oeuvres cinématographiques réalisées par des femmes.',
      query: `PREFIX wdt: <http://www.wikidata.org/prop/direct/>
PREFIX wd: <http://www.wikidata.org/entity/>
PREFIX frbroo: <http://iflastandards.info/ns/fr/frbr/frbroo/>
PREFIX crm: <http://www.cidoc-crm.org/cidoc-crm/>
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX cmtqo: <http://data.cinematheque.qc.ca/ontology/cmtqo/0.1#>
PREFIX cmtq: <${baseUri}>

SELECT DISTINCT (COUNT(DISTINCT ?workDirectedByFemale) as ?workDirectedByFemaleCount) WHERE {
?workDirectedByFemale a frbroo:F21_Recording_Work .
?recordingEvent frbroo:R22_created_a_realization_of ?workDirectedByFemale .
?recordingEvent crm:P9_consists_of ?roleActivity .
?roleActivity cmtqo:in_the_role_of cmtq:Role1 .
?roleActivity crm:P14_carried_out_by ?director .
?director owl:sameAs ?directorLinkWikidata .
SERVICE <https://query.wikidata.org/bigdata/namespace/wdq/sparql> {
  ?directorLinkWikidata wdt:P21 wd:Q6581072 .
}
}`
    },
    {
      name: 'Les oeuvres cinématographiques dans une des langues Inuit.',
      query: `PREFIX wdt: <http://www.wikidata.org/prop/direct/>
PREFIX wd: <http://www.wikidata.org/entity/>
PREFIX frbroo: <http://iflastandards.info/ns/fr/frbr/frbroo/>
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX cmtqo: <http://data.cinematheque.qc.ca/ontology/cmtqo/0.1#>
PREFIX cmtq: <${baseUri}>

SELECT DISTINCT * WHERE {
?recordingWork a frbroo:F21_Recording_Work .
?recordingWork wdt:P407 ?language .
?language a cmtqo:Language .
{ ?language owl:sameAs ?languageWikidataLink . } UNION { ?language owl:sameAs wd:Q27796 }
SERVICE <https://query.wikidata.org/bigdata/namespace/wdq/sparql> {
  { ?languageWikidataLink wdt:P279 wd:Q27796 . }
}
}`
    },
    {
      name: 'Les oeuvres cinématographiques produites par la société « Les Productions du Verseau Inc. »',
      query: `PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX wdt: <http://www.wikidata.org/prop/direct/>
PREFIX wd: <http://www.wikidata.org/entity/>
PREFIX frbroo: <http://iflastandards.info/ns/fr/frbr/frbroo/>
PREFIX crm: <http://www.cidoc-crm.org/cidoc-crm/>
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX cmtqo: <http://data.cinematheque.qc.ca/ontology/cmtqo/0.1#>
PREFIX cmtq: <${baseUri}>

SELECT DISTINCT ?recordingWork ?recordingWorkLabel WHERE {
?recordingWork a frbroo:F21_Recording_Work .
?recordingWork rdfs:label ?recordingWorkLabel .
?productionEvent frbroo:R22_created_a_realization_of ?recordingWork .
?productionEvent crm:P9_consists_of ?roleActivity .
?roleActivity cmtqo:in_the_role_of cmtq:Role34 . # Société de production
?roleActivity crm:P14_carried_out_by cmtq:LegalBody9474 . # Les Productions du Verseau Inc.
}`
    }
  ]
}

module.exports = baseuri => router => {
  router.get('/doc/sparql', async (ctx, next) => {
    const hostUri = `${ctx.protocol}://${ctx.host}`
    const resourceBaseUri = url.resolve(baseuri, 'resource/')

    try {
      const exampleQueries = await cqrdf.getSparqlQueryExamples()
      await ctx.render('sparql', { hostUri, resourceBaseUri, exampleQueries })
    } catch {
      await ctx.render('sparql', { hostUri, resourceBaseUri, exampleQueries: [] })
    }
  })
}
