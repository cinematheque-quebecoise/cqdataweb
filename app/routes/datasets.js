const path = require('path')
const url = require('url')
const fs = require('fs').promises
const stream = require('stream')
const factory = require('@graphy/core.data.factory');

const rdf = require('./rdf')
const cqrdf = require('./cqrdf')
const appareils = require('./appareils')

module.exports = baseuri => router => {

  const resourceMiddleware = (pageBaseUri, dataBaseUri) => async (ctx, next) => {
    const mimetype = ctx.accepts(['application/x-turtle', 'application/rdf+xml', 'text/plain', 'html'])
    if (!mimetype) {
      await next
    } else if (mimetype && mimetype === 'html') {
      ctx.set({
        'Location': `${pageBaseUri}/${ctx.params.resourceId}`
      })
    } else {
      ctx.set({
        'Location': `${dataBaseUri}/${ctx.params.resourceId}`
      })
    }
    ctx.status = 303
  }

  router.get('/resource/:resourceId', resourceMiddleware('/page', '/data'))
  router.get('/void/:resourceId', resourceMiddleware('/void/page', '/void/data'))
  router.get('/void/Dataset/:resourceId', resourceMiddleware('/void/page/Dataset', '/void/data/Dataset'))
  router.get('/void/Dataset/CinemathequeQuebecoiseLinkedOpenDatabase/:resourceId', resourceMiddleware("/void/page/Dataset/CinemathequeQuebecoiseLinkedOpenDatabase", "/void/data/Dataset/CinemathequeQuebecoiseLinkedOpenDatabase"))

  const resourceDataMiddleware = entityBaseUri => async (ctx, next) => {
    const mimeType = ctx.accepts(['text/turtle', 'application/x-turtle', 'application/rdf+xml', 'application/n-triples'])
    const extension = path.extname(ctx.params.resourceId)
    const extensionMimeType = rdf.getRdfMimeByExt(extension)

    const chosenMimeType = extensionMimeType === undefined ? mimeType : extensionMimeType
    const writer = rdf.getWriterByMime(chosenMimeType)

    const prefixes = await rdf.fetchPrefixes()

    if (writer === undefined) {
      await next
    } else {
      const resourceId = path.basename(ctx.params.resourceId, extension)
      const resourceUriPrefix = url.resolve(baseuri, `${entityBaseUri}`)
      const entityUri = url.resolve(resourceUriPrefix, `${entityBaseUri}${resourceId}`)
      const sparqlEndpoint = `${ctx.protocol}://${ctx.host}/sparql`
      const triplesWithSubjectAsEntity = await rdf.fetchTriplesBySubjectEntity(sparqlEndpoint, entityUri)
      const triplesWithObjectAsEntity = await rdf.fetchTriplesByObjectEntity(sparqlEndpoint, entityUri)
      const triples = triplesWithSubjectAsEntity.concat(triplesWithObjectAsEntity)

      const prefixMappingsSubset = {
        ...rdf.getPrefixMappingsSubset(triples, prefixes),
        cmtq: resourceUriPrefix
      }
      const replaceByPrefix = rdf.replaceUriWithPrefix(prefixMappingsSubset)

      const statements = triples.reduce((acc, triple) => {
        const prefixedSubject = replaceByPrefix(triple.subjectNode.value)
        const subject = prefixedSubject === undefined ? `>${triple.subjectNode.value}` : prefixedSubject

        const prefixedPredicate = replaceByPrefix(triple.predicateNode.value)
        const predicate = prefixedPredicate === undefined ? `>${triple.predicateNode.value}` : prefixedPredicate

        const prefixedObject = triple.objectNode.type === 'uri' ? replaceByPrefix(triple.objectNode.value)
                                                                : `"${triple.objectNode.value}`
        const object = prefixedObject === undefined ? `>${triple.objectNode.value}` : prefixedObject

        if (acc[subject] === undefined) {
          acc[subject] = {
            [predicate]: [object]
          }
        } else {
          if (acc[subject][predicate] === undefined) {
            acc[subject] = {
              ...acc[subject],
              [predicate]: [object]
            }
          } else {
            acc[subject][predicate] = acc[subject][predicate].concat([object])
          }
        }
        return acc
      }, {})

      const s = new stream.PassThrough()
      if (triples.length > 0) {
        let ds_writer = writer({ prefixes: prefixMappingsSubset })
        ds_writer.pipe(s)
        ds_writer.write({
          type: 'c3r',
          value: statements
        })

        ds_writer.end(() => {
          // s.push(null)
        })

        ctx.set('Content-Type', chosenMimeType)
        ctx.body = s
      } else {
        await next
      }
    }
  }

  router.get('/data/:resourceId', resourceDataMiddleware("/resource/"))
  router.get('/void/data/:resourceId', resourceDataMiddleware("/void/"))
  router.get('/void/data/Dataset/:resourceId', resourceDataMiddleware("/void/Dataset/"))
  router.get('/void/data/Dataset/CinemathequeQuebecoiseLinkedOpenDatabase/:resourceId', resourceDataMiddleware("/void/Dataset/CinemathequeQuebecoiseLinkedOpenDatabase/"))

  const resourcePageMiddleware = (entityBaseUri, entityDataBaseUri) => async (ctx, next) => {
    const entityUri = url.resolve(baseuri, `${entityBaseUri}${ctx.params.resourceId}`)
    const entityDataUri = url.resolve(baseuri, `${entityDataBaseUri}${ctx.params.resourceId}`)
    const sparqlEndpoint = `${ctx.protocol}://${ctx.host}/sparql`

    const limit = 10
    const page = parseInt(ctx.query.page)
    const pageNumber = isNaN(page) ? 1 : page < 1 ? 1 : page
    const offset = limit * (pageNumber - 1)
    const maxResults = await rdf.countTriplesByObjectEntity(sparqlEndpoint, entityUri)
    const maxPageNumber = Math.ceil(maxResults / limit)
    const pagination = {
      currentPage: pageNumber,
      maxPageNumber: maxPageNumber
    }

    const triplesWithSubjectAsEntity = await rdf.fetchTriplesBySubjectEntity(sparqlEndpoint, entityUri)
    const triplesWithObjectAsEntity = await rdf.fetchTriplesByObjectEntity(sparqlEndpoint, entityUri, limit, offset)
    const triples = triplesWithSubjectAsEntity.concat(triplesWithObjectAsEntity)

    const prefixes = await rdf.fetchPrefixes()

    const prefixMappings = {
      ...prefixes,
      cmtq: url.resolve(baseuri, 'resource/')
    }

    const prefixMappingsSubset = rdf.getPrefixMappingsSubset(triples, prefixMappings)

    if (triplesWithSubjectAsEntity.length > 0) {
      const resourceLabel = triplesWithSubjectAsEntity.find(t => t.predicateNode.value === 'http://www.w3.org/2000/01/rdf-schema#label')
      const resourceUri = `${ctx.protocol}://${ctx.host}${entityBaseUri}${ctx.params.resourceId}`
      await ctx.render('resource', {
        resourceLabel: resourceLabel === undefined ? resourceUri : resourceLabel.objectNode.value,
        resourceUri: resourceUri,
        resourceDataBaseUri: entityDataUri,
        triplesWithSubjectAsEntity,
        triplesWithObjectAsEntity,
        prefixMappings: prefixMappingsSubset,
        replaceUriWithPrefix: rdf.replaceUriWithPrefix(prefixMappings),
        pagination
      })
    } else {
      await next()
    }
  }

  router.get('/page/:resourceId', resourcePageMiddleware("/resource/", "/data/"))
  router.get('/void/page/:resourceId', resourcePageMiddleware("/void/", "/void/data/"))
  router.get('/void/page/Dataset/:resourceId', resourcePageMiddleware("/void/Dataset/", "/void/data/Dataset/"))
  router.get('/void/page/Dataset/CinemathequeQuebecoiseLinkedOpenDatabase/:resourceId', resourcePageMiddleware("/void/Dataset/CinemathequeQuebecoiseLinkedOpenDatabase/", "/void/data/Dataset/CinemathequeQuebecoiseLinkedOpenDatabase/"))

  router.get('/doc/lod/schema', async (ctx, next) => {
    await ctx.render('lod-schema')
  })

  router.get('/doc/lod/datasets', async (ctx, next) => {

    try {
      const datasets = await cqrdf.getDatasetFilesPerDate()
      const datasetsWithoutCsv = datasets
        .map(d => {
          return {
            ...d,
            files: d.files.filter(f => !f.filename.includes("csv.tar.gz"))
          }
        })
        .sort((d1, d2) => d1.date < d2.date ? 1 : d1.date > d2.date ? -1 : 0)
      await ctx.render('lod-datasets', { datasets: datasetsWithoutCsv })
    } catch {
      await ctx.render('lod-datasets', { datasets: [] })
    }
  })

  router.get('/doc/datasets/cmtq-dataset:ext(.*)', async (ctx, next) => {
    const datasetName = `cmtq-dataset${ctx.params.ext}`
    try {
      const datasets = await cqrdf.getDatasetFilesPerDate()
      if (datasets.length == 0) {
        await next
      } else {
        const latestDataset = datasets.sort((d1, d2) => d1.date < d2.date ? 1 : d1.date > d2.date ? -1 : 0)[0]
        const fileOpt = latestDataset.files.find(f => f.filename == datasetName)
        if (fileOpt === undefined) {
          await next
        } else {
          ctx.set({
            'Location': fileOpt.url
          })
          ctx.status = 303
        }
        await next
      }
    } catch {
      await next
    }
  })

  router.get('/doc/datasets/appareils_cinematographiques:ext(.*)', async (ctx, next) => {
    const datasetName = `appareils_cinematographiques${ctx.params.ext}`
    try {
      const datasets = await appareils.getDatasetFiles()
      if (datasets.length == 0) {
        await next
      } else {
        const latestDataset = datasets.sort((d1, d2) => d1.date < d2.date ? 1 : d1.date > d2.date ? -1 : 0)[0]
        const fileOpt = latestDataset.files.find(f => f.filename == datasetName)
        if (fileOpt === undefined) {
          await next
        } else {
          ctx.set({
            'Location': fileOpt.url
          })
          ctx.status = 303
        }
        await next
      }
    } catch {
      await next
    }
  })

  router.get('/doc/datasets', async (ctx, next) => {
    const appareilsDatasets = await appareils.getDatasetFiles()
    const rdfCsvDatasets = await getLodCsvDatasets()
    await ctx.render('datasets', { appareilsDatasets, workDatasets: rdfCsvDatasets })
  })

  const getLodCsvDatasets = async () => {
    const rdfDatasets = await cqrdf.getDatasetFilesPerDate()
    if (rdfDatasets.length === 0) {
      return undefined
    }
    return rdfDatasets
      .sort((f1, f2) => f1.date < f2.date ? 1 : f1.date > f2.date ? -1 : 0)
      .map(f => {
        return {
          date: f.date,
          files: [f.files.find(f => f.filename.includes("csv"))]
        }
      })
  }
}
