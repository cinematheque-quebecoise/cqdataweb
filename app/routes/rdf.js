const fs = require('fs').promises
const path = require('path')
const axios = require('axios')
const ttl_write = require('@graphy/content.ttl.write')
const nt_write = require('@graphy/content.nt.write')
const xml_scribe = require('@graphy/content.xml.scribe')

class Node {
  constructor(type, value) {
    this.type = type
    this.value = value
  }
}

class Triple {
  constructor(subjectNode, predicateNode, objectNode) {
    this.subjectNode = subjectNode
    this.predicateNode = predicateNode
    this.objectNode = objectNode
  }
}

/**
 * Fetch triples from SPARQL endpoint where given an entity URI as subject.
 *
 * @param {string} sparqlEndpoint - URL of the SPARQL endpoint
 * @param {string} entityURI - URI of the entity
 * @return {Array<Triple>} Array of triples
 */
const fetchTriplesBySubjectEntity = async (sparqlEndpoint, entityUri) => {
  const query = `
SELECT ?property ?object WHERE {
  <${entityUri}> ?property ?object .
}
`
  const result = await axios.get(`${sparqlEndpoint}?query=${query}`)
  return result.data.results.bindings
    .map(result => {
      return new Triple(
        new Node('uri', entityUri),
        new Node(result.property.type, result.property.value),
        new Node(result.object.type, result.object.value))
    })
}

/**
 * Fetch triples from SPARQL endpoint where given an entity URI as object.
 *
 * @param {string} sparqlEndpoint - URL of the SPARQL endpoint
 * @param {string} entityURI - URI of the entity
 * @param {number} limit - Maximum number of results
 * @param {number} offset - Offset results
 * @return {Array<Triple>} Array of triples
 */
const fetchTriplesByObjectEntity = async (sparqlEndpoint, entityUri, limit, offset) => {
  const limitQuery = `LIMIT ${limit}`
  const offsetQuery = `OFFSET ${offset}`
  const paginationQuery = `
ORDER BY ?subject ?property
${limit === undefined ? "" : limitQuery}
${offset === undefined ? "" : offsetQuery}
`
  const query = `
SELECT ?subject ?property WHERE {
  ?subject ?property <${entityUri}> .
}
${limit === undefined ? "" : paginationQuery}
`
  const result = await axios.get(`${sparqlEndpoint}?query=${query}`)
  return result.data.results.bindings
    .map(result => {
      return new Triple(
        new Node(result.subject.type, result.subject.value),
        new Node(result.property.type, result.property.value),
        new Node('uri', entityUri))
    })
}

/**
 * Count triples from SPARQL endpoint where given an entity URI as object.
 *
 * @param {string} sparqlEndpoint - URL of the SPARQL endpoint
 * @param {string} entityURI - URI of the entity
 * @return {number} Number of triples
 */
const countTriplesByObjectEntity = async (sparqlEndpoint, entityUri) => {
  const query = `
SELECT (COUNT(DISTINCT *) as ?count) WHERE {
  ?subject ?property <${entityUri}> .
}
`
  const result = await axios.get(`${sparqlEndpoint}?query=${query}`)
  return result.data.results.bindings.map(result => result.count.value)[0]
}

/**
 * Returns all namespaces from triples
 *
 * @param {Array<Triple>} triples - Array of triples
 * @returns {Array<string>} Array of namespaces
 *
 * @example
 * // returns ["http://example.com/person/", "http://www.w3.org/2000/01/rdf-schema#"]
 * getNamespacesFromTriples([
 *   new Triple(
 *     new Node("uri", "http://example.com/person/1),
 *     new Node("uri", "http://www.w3.org/2000/01/rdf-schema#label"),
 *     new Node("literal", "Marc"))])
 */
const getNamespacesFromTriples = triples => {
  const nodes = [].concat(...triples
    .map(triple => [triple.subjectNode, triple.predicateNode, triple.objectNode])
  )
  const uris = nodes.filter(node => node.type === 'uri').map(node => node.value)

  return [...new Set(uris.map(uri => getNamespaceFromUri(uri)))]
}

/**
 * Given prefix mappings, shorten an URI with a prefix.
 *
 * @param {Object} prefixMappings - Dictionnary mapping prefix name to URI
 * @param {string} uri - URI to shorten
 * @return {string} Shortened URI
 *
 * @example
 * // returns "rdf:type"
 * replaceUriWithPrefix({ rdf: "http://www.w3.org/1999/02/22-rdf-syntax-ns#" })("http://www.w3.org/1999/02/22-rdf-syntax-ns#type")
 *
 * @example
 * // returns undefined
 * replaceUriWithPrefix({})("http://www.w3.org/1999/02/22-rdf-syntax-ns#type")
 */
const replaceUriWithPrefix = prefixMappings => {
  const namespaceToPrefix = createUriToPrefixObj(prefixMappings)
  return uri => {
    const namespace = getNamespaceFromUri(uri)
    if (namespace in namespaceToPrefix) {
      return `${namespaceToPrefix[namespace]}:${uri.replace(namespace, '')}`
    } else {
      return undefined
    }
  }
}

/**
 * Returns namespace from URI using a naive algorithm.
 *
 * @param {string} uri - URI to extract namespace
 * @returns {string} Namespace of URI
 *
 * @example
 * // returns "http://www.w3.org/1999/02/22-rdf-syntax-ns#"
 * getNamespaceFromUri("http://www.w3.org/1999/02/22-rdf-syntax-ns#type")
 *
 * @example
 * // returns "http://purl.org/dc/terms/"
 * getNamespaceFromUri "http://purl.org/dc/terms/creator"
 */
const getNamespaceFromUri = uri => {
  const hashSegments = uri.split('#')
  if (hashSegments.length === 2) {
    return hashSegments[0] + '#'
  } else {
    const segments = uri.split('/')
    if (segments.length > 1) {
      return segments.slice(0, segments.length - 1).join('/') + '/'
    } else {
      return undefined
    }
  }
}

/**
 * Returns subset of prefix mappings that appear in a list of triples.
 *
 * @params {Array} triples - List of triples
 * @params {Object} prefixMappings - Object from prefix to namespace
 *
 * @return Subset of prefix mappings
 *
 * @example
 * // returns { rdf: "http://www.w3.org/2000/01/rdf-schema#" }
 * let triples = [
 *   new Triple(
 *     new Node("uri", "http://example.com/person/1),
 *     new Node("uri", "http://www.w3.org/2000/01/rdf-schema#label"),
 *     new Node("literal", "Marc"))
 * ]
 * let prefixMappings = {
 *   rdf: "http://www.w3.org/1999/02/22-rdf-syntax-ns#",
 *   rdfs: "http://www.w3.org/2000/01/rdf-schema#"
 * }
 * getPrefixMappingsSubset(triples, prefixMappings)
 */
const getPrefixMappingsSubset = (triples, prefixMappings) => {
  const namespaceToPrefix = createUriToPrefixObj(prefixMappings)

  const namespaces = getNamespacesFromTriples(triples)

  return namespaces.reduce((acc, namespace) => {
    if (namespace in namespaceToPrefix) {
      acc[namespaceToPrefix[namespace]] = namespace
    }
    return acc
  }, {})
}

/**
 * Create a dictionnary mapping a URI to a prefix name.
 *
 * @param {Object} prefixMappings - Dictionnary mapping prefix name to URI
 * @return {Object} Dictionnary mapping a URI to a namespace name
 *
 * @example
 * // returns { "http://www.w3.org/1999/02/22-rdf-syntax-ns#": "rdf" }
 * createUriToPrefixObj({ rdf: "http://www.w3.org/1999/02/22-rdf-syntax-ns#" })
 */
const createUriToPrefixObj = prefixMappings => {
  return Object.keys(prefixMappings)
    .reduce((acc, prefix) => {
      acc[prefixMappings[prefix]] = prefix
      return acc
    }, {})
}

/**
 * Get Graphy writer by MIME type.
 *
 * @param {string} mimeType - Mime type
 */
const getWriterByMime = mimeType => {
  if (mimeType === 'application/x-turtle' || mimeType === 'text/turtle') {
    return ttl_write
  } else if (mimeType === 'application/rdf+xml') {
    return xml_scribe
  } else if (mimeType === 'application/n-triples') {
    return nt_write
  }
}

/**
 * Get RDF MIME type by extension.
 *
 * @param {string} extension - RDF extension
 * @return {string} Mime type
 *
 * @example
 * // returns "application/rdf+xml"
 * getRdfMimeByExt(".rdf")
 */
const getRdfMimeByExt = extension => {
  if (extension === '.rdf' || extension === '.xml') {
    return 'application/rdf+xml'
  } else if (extension === '.nt') {
    return 'application/n-triples'
  } else if (extension === '.ttl') {
    return 'text/turtle'
  }
}

const fetchPrefixes = async () => {
  // Fetched from https://prefix.cc/popular/all.file.json but TLS certificate expired
  const res = await fs.readFile(path.join(__dirname, '../public/prefixes.json'))
  return JSON.parse(res)
}

module.exports = {
  Triple,
  Node,
  fetchTriplesBySubjectEntity,
  fetchTriplesByObjectEntity,
  countTriplesByObjectEntity,
  getPrefixMappingsSubset,
  replaceUriWithPrefix,
  getWriterByMime,
  getRdfMimeByExt,
  fetchPrefixes
}
