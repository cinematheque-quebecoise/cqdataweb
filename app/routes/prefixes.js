module.exports = {
  "owl": "http://www.w3.org/2002/07/owl#",
  "frbroo": "http://iflastandards.info/ns/fr/frbr/frbroo/",
  "rdac": "http://rdaregistry.info/Elements/c/",
  "crm": "http://www.cidoc-crm.org/cidoc-crm/",
  "wd": "http://www.wikidata.org/entity/",
  "wdt": "http://www.wikidata.org/prop/direct/",
  "xsd": "http://www.w3.org/2001/XMLSchema#",
  "rdf": "http://www.w3.org/1999/02/22-rdf-syntax-ns#",
  "schema": "http://schema.org/",
  "dcterms": "http://purl.org/dc/terms/",
  "rdfs": "http://www.w3.org/2000/01/rdf-schema#",
  "foaf": "http://xmlns.com/foaf/0.1/",
  "dbo": "http://dbpedia.org/ontology/",
  "formats": "http://www.w3.org/ns/formats/",
  "void": "http://rdfs.org/ns/void#"
}
