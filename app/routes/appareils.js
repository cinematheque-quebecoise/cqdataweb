const gitlab = require('./gitlab')

const DATASET_GITLAB_PROJECT_ID = "25083708"

const getDatasetFiles = async () => {
  const datasets = await gitlab.fetchGitlabAssets(DATASET_GITLAB_PROJECT_ID)
  return datasets.map(d => {
    return {
      ...d,
      files: d.files.filter(f => f.filename.startsWith('appareil'))
    }
  })
}

module.exports = { getDatasetFiles }
