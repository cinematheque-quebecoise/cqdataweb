#!/usr/bin/env bash

# currentFolder=`pwd`/data
# # curl http://localhost:9998/blazegraph/sparql --data-urlencode "update=DROP ALL; LOAD <file://$currentFolder/Abox_Filmo_Pays.ttl>; LOAD <file://$currentFolder/Abox_Filmo_Realisation.ttl>; LOAD <file://$currentFolder/Abox_Filmo.ttl>; LOAD <file://$currentFolder/Abox_Generique.ttl>; LOAD <file://$currentFolder/Abox_Nom.ttl>;LOAD <file://$currentFolder/Abox_Pays.ttl>; LOAD <file://$currentFolder/Tbox.ttl>;"
# dataset=`realpath $1`
# # curl http://localhost:9998/blazegraph/sparql --data-urlencode "update=DROP ALL; LOAD <file://$currentFolder/Tbox.ttl>; LOAD <file://${dataset}>;"
# curl http://localhost:9998/blazegraph/sparql --data-urlencode "update=DROP ALL; LOAD <file://${dataset}>;"

HOST="$1"

dataset=`realpath $2`

echo $dataset

wait-for-url() {
    echo "Testing $1"
    timeout -s TERM 20 sh -c \
    'while [[ "$(curl -s -o /dev/null -L -w ''%{http_code}'' ${0})" != "200" ]];\
    do echo "Waiting for ${0}" && sleep 2;\
    done' ${1}
    curl $1 --data-urlencode "update=DROP ALL; LOAD <file://${dataset}>;"
}
wait-for-url ${HOST}
