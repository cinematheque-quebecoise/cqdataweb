const Koa = require('koa')
const Router = require('koa-router')
const app = new Koa()
const router = new Router()

const co = require('co')
const views = require('koa-views')
const Pug = require('koa-pug')
const mount = require('koa-mount')
const convert = require('koa-convert')
const compose = require('koa-compose')
const json = require('koa-json')
const onerror = require('koa-onerror')
const bodyparser = require('koa-bodyparser')
const logger = require('koa-logger')
const send = require('koa-send')
const debug = require('debug')('koa2:server')
const static = require('koa-static')
const locale = require('koa-locale')
const session = require('koa-session')
const serveIndex = require('koa2-serve-index')
const cors = require('@koa/cors')
// const i18n = require('koa-i18n')
const path = require('path')
const yargs = require('yargs')
// const serveIndex = require('serve-index')

const argv = yargs
    .option('baseuri', {
        alias: 'b',
        description: 'Base URI to use for RDF data',
        type: 'string',
    })
    .option('port', {
        alias: 'p',
        description: 'Port number',
        type: 'int',
    })
    .help()
    .alias('help', 'h')
    .argv;

if (argv.baseuri === undefined) {
  console.error("[ERROR]: Missing --baseuri|-b command line parameter.")
  process.exit(1)
}
if (argv.port === undefined) {
  console.error("[ERROR]: Missing --port|-p command line parameter.")
  process.exit(1)
}

if (process.env.APPAREILS_IMAGES_PATH === undefined) {
  console.error("[ERROR]: Missing appareils dataset path in environment variable APPAREILS_IMAGES_PATH")
  process.exit(1)
}

const i18n = require('./i18n.config.js')
const config = require('./config')
const routes = require('./routes')

const MarkdownIt = require('markdown-it')
const markdownItAnchor = require('markdown-it-anchor')
const markdownItSup = require('markdown-it-sup')
const md = new MarkdownIt({ html: true })
  .use(markdownItAnchor, {
    slugify: s => {
      const newString = String(s)
        .trim()
        // .toLowerCase()
        .replace(/\s+/g, '-')
      return encodeURIComponent(newString)
    }
  })
  .use(markdownItSup)

// const port = process.env.PORT || config.port

app.keys = ['SECRET KEY']

// error handler
onerror(app)

app.use(session(app))

const pug = new Pug({
  viewPath: path.join(__dirname, 'views'),
  basedir: path.join(__dirname, 'views'),
  locals: { md },
  filters: {
    'my-markdown-it': (text, options) => {
      return md.render(text)
    }
  },
  app: app
})

async function changeLanguage (ctx, next) {
  const currentLang = ctx.session.lang === undefined
                    ? ctx.request.getLocale()
                    : ctx.session.lang
  ctx.request.setLocale(currentLang)

  const newLang = ctx.request.query.lang
  if (newLang !== undefined) {
    ctx.session.lang = newLang
    ctx.request.setLocale(newLang)
  }

  ctx.state.t = ctx.request.t
  ctx.state.lang = ctx.request.getLocale()
  await next()
}

// middlewares
app
  .use(bodyparser())
  .use(json())
  .use(logger())
  // .use(cors({ 'origin': 'http://localhost:9999' }))
  // .use(cors({ 'origin': '*' }))
  .use(async function (ctx, next) {
    // ctx.response.set('Access-Control-Allow-Origin', '*');
    i18n.init(ctx.request, ctx.response)
    await next()
  })
  .use(changeLanguage)
  .use(router.routes())
  .use(router.allowedMethods())
  .use(static(__dirname + '/public'))
  // .use(mount('/images/appareils', static(__dirname + '/public/images/appareils')))
  // .use(mount('/images/appareils', serveIndex(__dirname + '/public/images/appareils', { 'icons': true })))
  .use(
    mount('/images/appareils',
      compose(
        [ static(process.env.APPAREILS_IMAGES_PATH),
          serveIndex(process.env.APPAREILS_IMAGES_PATH, { 'icons': true })
        ]
      )
    )
  )

// if (process.env.NODE_ENV !== undefined && process.env.NODE_ENV.toLowerCase() === 'production') {
//   app.use(require('koa-static')(__dirname + '/client/build'))
//   router.get('/', async (ctx, next) => {
//     await send(ctx, ctx.path, { root: __dirname + '/client/build/index.html' });
//   })
// }

// logger
app.use(async (ctx, next) => {
  const start = new Date()
  await next()
  const ms = new Date() - start
  console.log(`${ctx.method} ${ctx.url} - ${ms}`)
})

routes(argv.baseuri)(router)

// catch 404 and forward to error handler
app.use(async(ctx, next) => {
  try {
    await next()
    const status = ctx.status || 404
    if (status === 404) {
        ctx.throw(404)
    }
  } catch (err) {
    ctx.status = err.status || 500
    if (ctx.status === 404) {
      //Your 404.jade
      await ctx.render('error', { message: 'Page introuvable!' })
    } else {
      //other_error jade
      await ctx.render('error', { message: 'Erreur interne du serveur!' })
    }
  }
})

app.on('error', function(err, ctx) {
  console.log(err)
})

module.exports = app.listen(argv.port, () => {
  console.log(`Listening on http://localhost:${argv.port}`)
})
