extends layout

include header

block content
  +header('lod-schema')

  h1 Schéma des données

  h2 Introduction

  p Les données ouvertes et liées de la Cinémathèque québécoise utilisent l'ontologie FRBRoo, qui est dérivé des ontologies CIDOC CRM et FRBR, pour décrire les données. Nous utilisons CIDOC CRM puisque c'est le standard pour rendre interopérables les données entre des institutions culturelles. Quant à l'utilisation de FRBRoo, la Cinémathèque québécoise suit les recommandations de la FIAF pour structurer ces données selon le modèle proposé par FRBR. Le modèle de FRBR structure les données selon les concepts suivants: oeuvre, expression (variante), manifestation et item.

  p Dans cette page, nous allons décrire la structure des données RDF afin de vous permettre d'écrire des requêtes SPARQL. Ces requêtes SPARQL permettent d'interroger directement nos données ouvertes et liées.

  p À ce jour, les données ouvertes et liées de la Cinémathèque québécoise correspondent :

  ul
    li au générique de chaque œuvre cinématographique (toutes les personnes/organisations qui ont occupé une fonction particulière dans la production d'une œuvre cinématographique)
    li au titre, synopsis (en français et anglais), année de sortie, date de début et de fin de production, lieu de production, langue et genre de chaque œuvre cinématographique
    li au nom et prénom de chaque personne qui a participé à la production d'une œuvre
    li au nom de chaque organisation qui a participé à la production d'une œuvre
    li aux liens avec Wikidata pour les personnes, œuvres cinématographiques, langues, genres cinématographiques et les lieux

  p La figure ci-dessous présente la structure générale des données. Chaque rectangle peut représenter une classe (ex. frbroo:F1_Work) ou un type de littéral (ex. xsd:integer qui représente un nombre entier, ou xsd:string pour une chaîne de caractères). Pour chaque classe, nous notons optionnellement le type spécifique de classe avec l'utilisation de <em>crm:E55_Type</em> et <em>crm:P2_has_type</em>.

  figure
    img(src="/images/cmtqlod.svg", width="100%")
    figcaption Figure 1. Figure de la structure générale des données RDF de la Cinémathèque québécoise

  p Dans les prochaines sections, nous allons expliquer en détail les différentes parties de la figure 1 pour mieux connaître la structure interne des données. Nous utiliserons la syntaxe Turtle pour écrire le code RDF avec les préfixes définit dans la figure ci-haute.

  h2 Langue

  p Pour chaque langue dans les données, nous avons son identifiant unique qui correspond à sa clé primaire de la base de données relationnelle, son libellé et son concept équivalent de Wikidata. Voici des exemples de représentation pour la langue française et la langue anglaise:

  pre
    code.language-turtle.
      # Instance d'une langue: le français (identifiant 38)

      cmtq:Language38
        a crm:E56_Language ;
        rdfs:label "français"@fr ;
        rdfs:comment "La langue française"@fr ;
        # Entité équivalente dans Wikidata
        owl:sameAs wd:Q1860 ;
        # Appellation de la langue
        crm:P1_is_identified_by cmtq:AppellationLanguage38 ;
        # Numéro d'identifiant unique de la langue
        crm:P48_has_preferred_identifier cmtq:IdentifierLanguage38 ;
      .

      cmtq:AppellationLanguage38
        crm:P190_has_symbolic_content "français" ;
        rdfs:comment "Appellation de la langue cmtq:Language38"@fr ;
      .

      cmtq:IdentifierLanguage38
        crm:P190_has_symbolic_content "38" ;
        rdfs:comment "Identifiant principal de la langue cmtq:Language38"@fr ;
      .

      # Instance d'une langue: l'anglais (identifiant 8)

      cmtq:Language8
        a crm:E56_Language ;
        rdfs:label "anglais"@fr ;
        rdfs:comment "La langue anglaise"@fr ;
        # Entité équivalente dans Wikidata
        owl:sameAs wd:Q150 ;
        # Appellation de la langue
        crm:P1_is_identified_by cmtq:AppellationLanguage8 ;
        # Numéro d'identifiant unique de la langue
        crm:P48_has_preferred_identifier cmtq:IdentifierLanguage8 ;
      .

      cmtq:AppellationLanguage8
        crm:P190_has_symbolic_content "anglais" ;
        rdfs:comment "Appellation de la langue cmtq:Language8"@fr ;
      .

      cmtq:IdentifierLanguage8
        crm:P190_has_symbolic_content "8" ;
        rdfs:comment "Identifiant principal de la langue cmtq:Language8"@fr ;
      .

  h2 Lieu

  p Pour chaque lieu dans les données, nous avons son identifiant unique qui correspond à sa clé primaire de la base de données relationnelle, son libellé et son entité équivalente dans Wikidata. Voici des exemples de représentation pour le Québec et la Turquie:

  pre
    code.language-turtle.
      # Instance d'un lieu: Québec

      cmtq:Place216
        a crm:E53_Place ;
        rdfs:label "Canada : Québec"@fr ;
        rdfs:comment "Lieu identifié par « Canada : Québec »"@fr ;
        # Entité équivalente dans Wikidata
        owl:sameAs wd:Q176 ;
        # Appellation du lieu
        crm:P1_is_identified_by cmtq:AppellationPlace216 ;
        # Numéro d'identifiant unique du lieu
        crm:P48_has_preferred_identifier cmtq:IdentifierPlace216 ;
      .

      cmtq:AppellationPlace216
        crm:P190_has_symbolic_content "Canada : Québec" ;
        rdfs:comment "Appellation du lieu cmtq:Place216"@fr ;
      .

      cmtq:IdentifierPlace216
        crm:P190_has_symbolic_content "216" ;
        rdfs:comment "Identifiant principal du lieu cmtq:Place216"@fr ;
      .

      # Instance d'un lieu: Turquie

      cmtq:Place276
        a crm:E53_Place ;
        rdfs:label "Turquie"@fr ;
        rdfs:comment "Lieu identifié par « Turquie »"@fr ;
        # Entité équivalente dans Wikidata
        owl:sameAs wd:Q43 ;
        # Appellation du lieu
        crm:P1_is_identified_by cmtq:AppellationPlace276 ;
        # Numéro d'identifiant unique du lieu
        crm:P48_has_preferred_identifier cmtq:IdentifierPlace276 ;
      .

      cmtq:AppellationPlace276
        crm:P190_has_symbolic_content "Turquie" ;
        rdfs:comment "Appellation du lieu cmtq:Place276"@fr ;
      .

      cmtq:IdentifierPlace276
        crm:P190_has_symbolic_content "276" ;
        rdfs:comment "Identifiant principal du lieu cmtq:Place276"@fr ;
      .

  h2 Fonction

  p Le concept de « Fonction » représente la fonction/rôle qu'un agent/acteur a occupée pendant la production d'une œuvre cinématographique. Pour chaque fonction dans les données, nous avons son identifiant unique qui correspond à sa clé primaire de la base de données relationnelle et son libellé.

  p Étant donné qu'il n'existe pas une classe pour représenter la fonction d'une personne dans CIDOC CRM ou FRBRoo, nous allons représenter cette classe à l'aide de la classe <em>crm:E55_Type</em> de CIDOC CRM:

  pre
    code.language-turtle.
      cmtq:Role
        a crm:E55_Type ;
        rdfs:label "Role" ;
      .

  p En indiquant que le concept cmtq:Role est une instance de crm:E55_Type, nous allons pouvoir l'utiliser pour typer les instances de fonctions avec la propriété <em>crm:P2_has_type</em>:

  pre
    code.language-turtle.
      # Instance d'une fonction: la fonction d'images

      cmtq:Role14
        a crm:E55_Type ;
        rdfs:label "Images"@fr ;
        rdfs:comment "Fonction identifiée par « Images »"@fr ;
        # Cette ressource est une fonction
        crm:P2_has_type cmtq:Role ;
        # Numéro d'identifiant unique de la fonction
        crm:P48_has_preferred_identifier cmtq:IdentifierRole14 ;
      .

      cmtq:IdentifierRole14
        crm:P190_has_symbolic_content "14" ;
        rdfs:comment "Identifiant principal du lieu cmtq:Role14"@fr ;
      .

      # Instance d'une fonction: la fonction d'interprétation

      cmtq:Role15
        a crm:E55_Type ;
        rdfs:label "Interprétation"@fr ;
        rdfs:comment "Fonction identifiée par « Interprétation »"@fr ;
        # Cette ressource est une fonction
        crm:P2_has_type cmtq:Role ;
        # Numéro d'identifiant unique de la fonction
        crm:P48_has_preferred_identifier cmtq:IdentifierRole15 ;
      .

      cmtq:IdentifierRole15
        crm:P190_has_symbolic_content "15" ;
        cmtq:IdentifierRole15 rdfs:comment "Identifiant principal du lieu cmtq:Role15"@fr ;
      .

  h2 Genre cinématographique

  p Ce concept représente le genre cinématographique d'une œuvre cinématographique. Pour chaque genre cinématographique dans les données, nous avons son identifiant unique qui correspond à sa clé primaire de la base de données relationnelle, son libellé et son entité équivalente dans Wikidata.

  p Étant donné qu'il n'existe pas une classe pour représenter le genre d'une œuvre cinématographique dans CIDOC CRM ou FRBRoo, nous allons représenter cette classe à l'aide de la classe <em>crm:E55_Type</em> de CIDOC CRM:

  pre
    code.language-turtle.
      cmtq:GenreCategory
        a crm:E55_Type ;
        rdfs:label "GenreCategory" ;
        rdfs:comment "Genre cinématographique ou catégorie d'une œuvre"@fr ;
      .

  p En indiquant que le concept cmtq:GenreCategory est une instance de crm:E55_Type, nous allons pouvoir l'utiliser pour typer les instances de fonctions avec la propriété <em>crm:P2_has_type</em>:

  pre
    code.language-turtle.
      # Instance d'un genre cinématographique: le genre dramatique

      cmtq:GenreCategory19982
        a crm:E55_Type ;
        rdfs:label "DRAMATIQUE"@fr ;
        rdfs:comment "Genre ou catégorie identifié par « DRAMTIQUE »"@fr ;
        # Cette ressource est un genre cinématographique
        crm:P2_has_type cmtq:GenreCategory ;
        # Entité équivalente dans Wikidata
        owl:sameAs wd:Q130232 ;
        # Numéro d'identifiant unique du genre cinématographique
        crm:P48_has_preferred_identifier cmtq:IdentifierGenreCategory19982 ;
      .

      cmtq:IdentifierGenreCategory19982
        crm:P190_has_symbolic_content "19982" ;
        rdfs:comment "Identifiant principal du genre ou catégorie cmtq:GenreCategory19982"@fr ;
      .

  h2 Organisation

  p Pour chaque organisation/entité légale dans les données, nous avons son identifiant unique qui correspond à sa clé primaire de la base de données relationnelle et son libellé. Voici un exemple de représentation:

  pre
    code.language-turtle.
      # Instance d'une organisation: Productions Prisma (Québec)

      cmtq:LegalBody9551
        a crm:E40_Legal_Body ;
        rdfs:label "PRODUCTIONS PRISMA (QUÉBEC)" ;
        rdfs:comment "Entité légale identifiée par « PRODUCTIONS PRISMA (QUÉBEC)"@fr ;
        # Appellation de l'organisation
        crm:P1_is_identified_by cmtq:AppellationLegalBody9551 ;
        # Numéro d'identifiant unique de l'organisation
        crm:P48_has_preferred_identifier cmtq:IdentifierLegalBody9551 ;
      .

      cmtq:AppellationLegalBody9551
        crm:P190_has_symbolic_content "PRODUCTIONS PRISMA (QUÉBEC)" ;
        rdfs:comment "Appellation de l'entité légale cmtq:LegalBody9551"@fr ;
      .

      cmtq:IdentifierLegalBody9551
        crm:P190_has_symbolic_content "9551" ;
        rdfs:comment "Identifiant principal de l'entité légale cmtq:LegalBody9551"@fr ;
      .

  h2 Personne

  p Pour chaque personne dans les données, nous avons son identifiant unique qui correspond à sa clé primaire de la base de données relationnelle, son nom, son prénom et son entité équivalente dans Wikidata. Voici des exemples de représentation:

  pre
    code.language-turtle.
      # Instance d'une personne: Denys Arcand

      cmtq:Person15334
        a crm:E21_Person ;
        rdfs:label "Denys Arcand" ;
        # Appellation de la personne
        crm:P1_is_identified_by cmtq:AppellationPerson15334 ;
        # Numéro d'identifiant unique de la personne
        crm:P48_has_preferred_identifier cmtq:IdentifierPerson15334 ;
        foaf:familyName "Arcand" ;
        foaf:givenName "Denys" ;
      .

      cmtq:AppellationPerson15334
        crm:P190_has_symbolic_content "Denys Arcand" ;
        rdfs:comment "Appellation de la personne cmtq:Person15334"@fr ;
      .

      cmtq:IdentifierPerson15334
        crm:P190_has_symbolic_content "15334" ;
        rdfs:comment "Identifiant principal de la personne cmtq:Person15334"@fr ;
      .

      # Instance d'une personne: Michel Brault

      cmtq:Person16070
        a crm:E21_Person ;
        rdfs:label "Michel Brault" ;
        rdfs:comment "Personne identifiée par « Michel Brault »"@fr ;
        # Appellation de la personne
        crm:P1_is_identified_by cmtq:AppellationPerson16070 ;
        # Numéro d'identifiant unique de la personne
        crm:P48_has_preferred_identifier cmtq:IdentifierPerson16070 ;
        # Nom de famille de la personne
        foaf:familyName "Brault" ;
        # Prénom de la personne
        foaf:givenName "Michel" ;
      .

      cmtq:AppellationPerson16070
        crm:P190_has_symbolic_content "Michel Brault" .
        rdfs:comment "Appellation de la personne cmtq:Person16070"@fr .
      .

      cmtq:IdentifierPerson16070
        crm:P190_has_symbolic_content "16070" ;
        rdfs:comment "Identifiant principal de la personne cmtq:Person16070"@fr ;
      .

  h2 Oeuvre cinématographique

  p Toutes les œuvres cinématographiques sont des instances de la classe `frbroo:F1_Work` (ex. « Les Invasions Barbares » une oeuvre). Pour chaque œuvre dans les données, nous avons son identifiant unique qui correspond à sa clé primaire de la base de données relationnelle, son titre original, des titres alternatifs, son genre cinématographique et son entité équivalente dans Wikidata. Voici un exemple de représentation d'une œuvre:

  pre
    code.language-turtle.
      # Instance d'une œuvre: Les Invasions Barbares (identifiant 63563)

      cmtq:Work63563
        a frbroo:F1_Work ;
        # Libellé de la ressource
        rdfs:label "LES INVASIONS BARBARES" ;
        rdfs:comment "œuvre identifiée par « LES INVASIONS BARBARES »"@fr ;
        # Genre cinématographique de l'œuvre
        crm:P2_has_type cmtq:GenreCategory14177 ;
        # Entité équivalente dans Wikidata
        owl:sameAs wd:Q549012 ;
        # Titre original de l'œuvre
        crm:P102_has_title cmtq:OriginalTitleWork63563 ;
        # Titre anglais de l'œuvre
        crm:P102_has_title cmtq:WorkTitle124696 ;
        # Numéro d'identifiant unique de l'œuvre
        crm:P48_has_preferred_identifier cmtq:IdentifierWork63563 ;
      .

      cmtq:OriginalTitleWork63563
        a crm:E35_Title ;
        crm:P190_has_symbolic_content "LES INVASIONS BARBARES" ;
        rdfs:comment "Titre original de l'œuvre cmtq:Work63563"@fr ;
      .

      cmtq:WorkTitle124696
        a crm:E35_Title ;
        crm:P190_has_symbolic_content "The Barbarian Invasions" ;
        rdfs:comment "Titre de l'œuvre cmtq:Work63563"@fr ;
      .

      cmtq:IdentifierWork63563
        crm:P190_has_symbolic_content "63563" ;
        rdfs:comment "Identifiant principal de l'œuvre cmtq:Work63563"@fr ;
      .

  h3 Budget de l'œuvre cinématographique

  p Le concept de budget de l'œuvre cinématographique représente le coût estimé de production.

  p Étant donné qu'il n'existe pas une classe pour représenter le budget d'une œuvre cinématographique dans CIDOC CRM ou FRBRoo, nous allons représenter cette classe à l'aide de la classe crm:E55_Type de CIDOC CRM:

  pre
    code.language-turtle.
      # Concept de budget
      cmtq:Budget
        a crm:E55_Type ;
        rdfs:label "Budget" ;
      .

      # Représentation du dollar canadien
      cmtq:CanadianDollar a crm:E98_Currency .

  p En indiquant que le concept cmtq:Budget est une instance de crm:E55_Type, nous allons pouvoir l'utiliser pour typer les instances de budget avec la propriété crm:P2_has_type:

  pre
    code.language-turtle.
      # Budget l'œuvre « DIARY OF A HIT MAN »
      cmtq:Work7 crm:P43_has_dimension cmtq:Dimension2300000CAD .

      cmtq:Dimension2300000CAD a crm:E54_Dimension .
      # Cette ressource est un budget
      cmtq:Dimension2300000CAD crm:P2_has_type cmtq:Budget .
      cmtq:Dimension2300000CAD rdfs:comment "Montant de 2300000 avec la monnaie cmtq:CanadianDollar"@fr .
      # Le montant du budget
      cmtq:Dimension2300000CAD crm:P181_has_amount "2300000"^^xsd:double .
      # La devise du montant du budget
      cmtq:Dimension2300000CAD crm:P180_has_currency cmtq:CanadianDollar .

  h2 Enregistrement d'une œuvre cinématographique

  p Pour chaque œuvre cinématographique, il y a un événement qui correspond à l'enregistrement de cette œuvre. Pour représenter cela, nous utiliserons les concepts <em>frbroo:F21_Recording_Work</em> (œuvre), <em>frbroo:F29_Recording_Event</em> (événement) et <em>frbroo:F26_Recording</em> (expression).

  pre
    code.language-turtle.
      # Événement d'enregistrement de l'œuvre « Les Invasions Barbares » (identifiant 63563)

      cmtq:RecordingEvent63563
        a frbroo:F29_Recording_Event ;
        rdfs:comment "Événement de création de l'œuvre cmtq:RecordingWork63563"@fr ;
        # L'événement a réalisé l'enregistrement de l'œuvre
        frbroo:R22_created_a_realisation_of cmtq:RecordingWork63563 ;
        # L'événement produit un enregistrement (une expression de l'œuvre)
        frbroo:R21_created cmtq:Recording63563 ;
      .

      # L'œuvre qui représente l'enregistrement est dérivée de l'œuvre cinématographique

      cmtq:RecordingWork63563
        a frbroo:F21_Recording_Work ;
        rdfs:comment "œuvre qui représente l'enregistrement de l'œuvre cmtq:Work63563"@fr ;
        # œuvre enregistrée est dérivée de l'œuvre originale
        frbroo:R2_is_derivative_of cmtq:Work63563 ;
      .

      # L'expression de la publication de l'œuvre incorpore l'enregistrement produit par l'événement d'enregistrement de l'œuvre

      cmtq:Recording63563
        a frbroo:F26_Recording ;
        rdfs:comment "Expression de l'œuvre cmtq:RecordingWork63563"@fr ;
      .

  h3 Pays de production

  p Le pays de production de l'œuvre est associé à l'instance de <em>frbroo:F29_Recording_Event</em> de l'œuvre.

  pre
    code.language-turtle.
      # Lieu de production de l'œuvre « Les Invasions Barbares » (identifiant 63563)

      cmtq:RecordingEvent63563 crm:P7_took_place_at cmtq:Place216 .

  h3 Date de production

  p La date de début et de fin de production de l'œuvre est associée à l'instance de <em>frbroo:F29_Recording_Event</em> de l'œuvre.

  pre
    code.language-turtle.
      # Description de l'enregistrement de l'œuvre identifié 63563 (« Les Invasions Barbares »)

      cmtq:RecordingEvent63563 crm:P4_has_time-span cmtq:Time-Span2002_2003 .

      cmtq:Time-Span2002-2003
        a crm:E52_Time-Span ;
        # Date de début
        crm:P82a_begin_of_the_begin "2002-01-01T00:00:00Z"^^xsd:dateTime ;
        # Date de fin
        crm:P82b_end_of_the_end "2003-01-01T00:00:00Z"^^xsd:dateTime ;
      .

      # Description de l'enregistrement de l'œuvre identifié 4409 (« Alisée »)

      cmtq:RecordingEvent4409 crm:P4_has_time-span cmtq:Time-Span1990-10-16_1990-11-16 .

      cmtq:Time-Span1990-10-16_1990-11-16
        a crm:E52_Time-Span ;
        # Date de début
        crm:P82a_begin_of_the_begin "1990-10-16T00:00:00Z"^^xsd:dateTime ;
        # Date de fin
        crm:P82b_end_of_the_end "1990-11-16T00:00:00Z"^^xsd:dateTime ;
      .

  h3 Langues d'un film

  p Les langues d'un film correspondent aux langues parlées dans les scènes du film. La langue sera associée à une expression qui correspond à l'enregistrement de l'œuvre.

  pre
    code.language-turtle.
      # Langue parlée dans l'œuvre « Les Invasions Barbares » (identifiant 63563)

      cmtq:Recording63563 crm:P72_has_language cmtq:Language38 .

  h2 Publication de l'œuvre cinématographique

  p Dans cette section, nous allons décrire comment on représente les concepts qui entourent la publication de l'œuvre cinématographique en utilisant les concepts <em>frbroo:F24_Publication_Expression</em>, <em>frbroo:F30_Publication_Event</em> et <em>crm:E7_Activity</em>.

  h3 Date de sortie en salle d'une œuvre cinématographique

  p La date de sortie en salle d'une œuvre cinématographique est associée à une instance de <em>crm:E7_Activity</em>.

  pre
    code.language-turtle.
      # L'événement de publication de l'enregistrement de l'œuvre « Les Invasions Barbares » (identifiant 63563)

      cmtq:PublicationEvent63563
        a frbroo:F30_Publication_Event ;
        rdfs:comment "Événement de publication de l'œuvre cmtq:RecordingWork63563"@fr ;
        # L'événement crée une expression publiée de l'œuvre
        frbro:R24_created cmtq:PublicationExpression63563 ;
        # L'événement se termine avant le début de la première projection publique de l'œuvre
        crm:P183_ends_before_the_start_of cmtq:PublicProjectionEvent63563 ;
      .

      # L'expression de publication de l'œuvre « Les Invasions Barbares »

      cmtq:PublicationExpression63563
        a frbroo:F24_Publication_Expression ;
        # L'expression de publication de l'œuvre incorpore l'enregistrement de l'œuvre
        crm:P165_incorporates cmtq:Recording63563 ;
        rdfs:comment "Expression de la publication de l'engistrement de l'œuvre cmtq:RecordingWork63563"@fr ;
      .

      # L'événement qui correspond à la première projection ou diffusion publique de l'œuvre cinématographique « Les Invasions Barbares ».
      # La première projection utilise une expression particulière de l'œuvre en question

      cmtq:PublicProjectionEvent a crm:E55_Type .

      cmtq:PublicProjectionEvent63563
        a crm:E7_Activity ;
        # Cette ressource est un événement de projection publique
        crm:P2_has_type cmtq:PublicProjectionEvent ;
        rdfs:comment "Événement de première projection l'expression cmtq:PublicationExpression63563"@fr ;
        # L'événement utilise une expression publiée de l'œuvre
        crm:P16_used_specific_object cmtq:PublicationExpression63563 ;
        # L'événement se déroule à un moment particulier
        crm:P4_has_time-span cmtq:Time-Span2003-01-01T00:00:00Z ;
      .

      cmtq:Time-Span2003
        a crm:E52_Time-Span ;
        # Date de début
        crm:P82a_begin_of_the_begin "2003-01-01T00:00:00Z"^^xsd:dateTime ;
      .

  h3 Durée originale

  p La durée originale correspond à la durée de l'œuvre au moment de sa sortie ou de sa première diffusion. La durée d'une œuvre est associée à une manifestation de l'enregistrement publié.

  pre
    code.language-turtle.
      # Manifestation de l'œuvre « Les Invasions Barbares » (identifiant 63563)
      cmtq:ManifestationProductType63563
        a frbroo:F3_Manifestation_Product_Type ;
        rdfs:comment "Manifestation qui représente l'ensemble des items de l'œuvre 63563 associés à sa première sortie ou première diffusion"@fr ;
        # L'expression qui est contenu dans la manifestation
        frbroo:CLR6_should_carry cmtq:PublicationExpression63563 ;
        # La durée de l'œuvre cinématographique encodée dans la manifestation
        crm:P43_has_dimension cmtq:Dimension6720Seconds ;
      .

      cmtq:Duration a crm:E55_Type .
      cmtq:Second a crm:E58_Measurement_Unit .

      cmtq:Dimension6720Seconds
        a crm:E54_Dimension ;
        # Cette ressource est une durée
        crm:P2_has_type cmtq:Duration ;
        # Valeur de la durée
        crm:P90_has_value "6720"^^xsd:integer ;
        # Unité de mesure de la durée
        crm:P91_has_unit cmtq:Second ;
      .

  h2 Activité de production

  p Dans chaque événement d'enregistrement d'une œuvre, des agents occupent des fonctions particulières. Voici une description de données où deux agents (une personne et une organisation) ont participé dans la production de l'œuvre « Les Invasions Barbares » (identifiant 36563):

  pre
    code.language-turtle.
      # Voici deux activités dans l'événement d'enregistrement de l'œuvre « Les Invasions Barbares » (identifiant 63563)

      cmtq:RecordingEvent36563
        crm:P9_consists_of cmtq:RecordingActivity3682 ;
        crm:P9_consists_of cmtq:RecordingActivity14377 ;
      .

      # Activité avec la fonction « Interprétation »

      cmtq:RecordingActivity3682
        a crm:E7_Activity ;
        rdfs:comment "Activité effectuée par cmtq:Person155788 dans la fonction cmtq:Role15 qui se déroule dans l'événement d'enregistrement de l'œuvre cmtq:RecordingWork108486"@fr ;
      .

      # Ressource intermédiaire qui permet d'encoder la fonction occupée par une
      # personne dans une activité particulière
      cmtq:RecordingActivityCarriedOutBy3682
        crm:P01_has_domain cmtq:RecordingActivity3682 ;
        crm:P02_has_range cmtq:Person155788 ;
        crm:P14.1_in_the_role_of cmtq:Role15 .
      .

      # Activité avec la fonction « Société de production »

      cmtq:RecordingActivity14377
        a crm:E7_Activity ;
        rdfs:comment "Activité effectuée par cmtq:LegalBody16537 dans la fonction cmtq:Role34 qui se déroule dans l'événement d'enregistrement de l'œuvre cmtq:RecordingWork108486"@fr ;
      .

      # Ressource intermédiaire qui permet d'encoder la fonction occupée par une
      # organisation dans une activité particulière
      cmtq:RecordingActivityCarriedOutBy14377
        crm:P01_has_domain cmtq:RecordingActivity14377 ;
        crm:P02_has_range cmtq:LegalBody16537 ;
        crm:P14.1_in_the_role_of cmtq:Role34 ;
      .
