Markdown documentation created by [pyLODE](http://github.com/rdflib/pyLODE) 2.8.3

# Cinematheque ontology

## Metadata
* **URI**
  * `http://data.cinematheque.qc.ca/cmtq`
* **Ontology RDF**
  * RDF ([cmtqo.ttl](turtle))

## Table of Contents
1. [Classes](#classes)
1. [Object Properties](#objectproperties)
1. [Datatype Properties](#datatypeproperties)
1. [Namespaces](#namespaces)
1. [Legend](#legend)


## Overview

**Figure 1:** Ontology overview
## Classes
[Country](#Country),
[CountryAppellation](#CountryAppellation),
[E21_Person](#E21_Person),
[E35_Title](#E35_Title),
[E41_Appellation](#E41_Appellation),
[E52_Time-Span](#E52_Time-Span),
[E53_Place](#E53_Place),
[E55_Type](#E55_Type),
[E61_Time_Primitive](#E61_Time_Primitive),
[E7_Activity](#E7_Activity),
[F1_Work](#F1_Work),
[F21_Recording_Work](#F21_Recording_Work),
[F29_Recording_Event](#F29_Recording_Event),
[Function](#Function),
[ProductionRoleActivity](#ProductionRoleActivity),
[Production_Event](#Production_Event),
[PublicWorkRelease](#PublicWorkRelease),
[RecordingWork](#RecordingWork),
[RecordingWorkType](#RecordingWorkType),
### CountryAppellation
Property | Value
--- | ---
URI | `http://data.cinematheque.qc.ca/cmtq#CountryAppellation`
In range of |[crm:P87_is_identified_by](http://www.cidoc-crm.org/cidoc-crm/P87_is_identified_by) (op)<br />
### Function
Property | Value
--- | ---
URI | `http://data.cinematheque.qc.ca/cmtq#Function`
Super-classes |[crm:E55_Type](http://www.cidoc-crm.org/cidoc-crm/E55_Type) (c)<br />
In domain of |[cmtq:cq_id](cq_id) (dp)<br />[rdfs:label](http://www.w3.org/2000/01/rdf-schema#label) (dp)<br />
In range of |[cmtq:has_function](has_function) (op)<br />
### ProductionRoleActivity
Property | Value
--- | ---
URI | `http://data.cinematheque.qc.ca/cmtq#ProductionRoleActivity`
Super-classes |[crm:E7_Activity](http://www.cidoc-crm.org/cidoc-crm/E7_Activity) (c)<br />
In domain of |[cmtq:participated_in](participated_in) (op)<br />[cmtq:executed_by](executed_by) (op)<br />[cmtq:has_function](has_function) (op)<br />
### Production_Event
Property | Value
--- | ---
URI | `http://data.cinematheque.qc.ca/cmtq#Production_Event`
Super-classes |[frbroo:F29_Recording_Event](http://iflastandards.info/ns/fr/frbr/frbroo/F29_Recording_Event) (c)<br />
In domain of |[crm:P4_has_time-span](http://www.cidoc-crm.org/cidoc-crm/P4_has_time-span) (op)<br />[cmtq:realises](realises) (op)<br />[cmtq:country_realisation](country_realisation) (op)<br />
In range of |[cmtq:participated_in](participated_in) (op)<br />
### PublicWorkRelease
Property | Value
--- | ---
URI | `http://data.cinematheque.qc.ca/cmtq#PublicWorkRelease`
Super-classes |[crm:E7_Activity](http://www.cidoc-crm.org/cidoc-crm/E7_Activity) (c)<br />
In domain of |[crm:P4_has_time-span](http://www.cidoc-crm.org/cidoc-crm/P4_has_time-span) (op)<br />[crm:P7_took_place_at](http://www.cidoc-crm.org/cidoc-crm/P7_took_place_at) (op)<br />
In range of |[crm:P67_refers_to](http://www.cidoc-crm.org/cidoc-crm/P67_refers_to) (op)<br />
### RecordingWork
Property | Value
--- | ---
URI | `http://data.cinematheque.qc.ca/cmtq#RecordingWork`
Super-classes |[frbroo:F21_Recording_Work](http://iflastandards.info/ns/fr/frbr/frbroo/F21_Recording_Work) (c)<br />
In domain of |[cmtq:cq_id](cq_id) (dp)<br />[crm:P102_has_title](http://www.cidoc-crm.org/cidoc-crm/P102_has_title) (op)<br />[crm:P67_refers_to](http://www.cidoc-crm.org/cidoc-crm/P67_refers_to) (op)<br />[frbroo:R2_is_derivative_of](http://iflastandards.info/ns/fr/frbr/frbroo/R2_is_derivative_of) (op)<br />[crm:P2_has_type](http://www.cidoc-crm.org/cidoc-crm/P2_has_type) (op)<br />
In range of |[cmtq:realises](realises) (op)<br />
### RecordingWorkType
Property | Value
--- | ---
URI | `http://data.cinematheque.qc.ca/cmtq#RecordingWorkType`
Super-classes |[crm:E55_Type](http://www.cidoc-crm.org/cidoc-crm/E55_Type) (c)<br />
In domain of |[rdfs:label](http://www.w3.org/2000/01/rdf-schema#label) (dp)<br />
In range of |[crm:P2_has_type](http://www.cidoc-crm.org/cidoc-crm/P2_has_type) (op)<br />
### Country
Property | Value
--- | ---
URI | `http://dbpedia.org/ontology/Country`
Super-classes |[crm:E53_Place](http://www.cidoc-crm.org/cidoc-crm/E53_Place) (c)<br />
In domain of |[crm:P87_is_identified_by](http://www.cidoc-crm.org/cidoc-crm/P87_is_identified_by) (op)<br />
In range of |[cmtq:country_realisation](country_realisation) (op)<br />
### F1_Work
Property | Value
--- | ---
URI | `http://iflastandards.info/ns/fr/frbr/frbroo/F1_Work`
In range of |[frbroo:R2_is_derivative_of](http://iflastandards.info/ns/fr/frbr/frbroo/R2_is_derivative_of) (op)<br />
### F21_Recording_Work
Property | Value
--- | ---
URI | `http://iflastandards.info/ns/fr/frbr/frbroo/F21_Recording_Work`
Description | <p>Recording Work</p>
Sub-classes |[cmtq:RecordingWork](RecordingWork) (c)<br />
### F29_Recording_Event
Property | Value
--- | ---
URI | `http://iflastandards.info/ns/fr/frbr/frbroo/F29_Recording_Event`
Description | <p>Recording Event</p>
Sub-classes |[cmtq:Production_Event](Production_Event) (c)<br />
### E21_Person
Property | Value
--- | ---
URI | `http://www.cidoc-crm.org/cidoc-crm/E21_Person`
Description | <p>Person</p>
Restrictions |[ub1bL259C49](ub1bL259C49) **some** [owl:Thing](http://www.w3.org/2002/07/owl#Thing) (c)<br />
In domain of |[foaf:gender](http://xmlns.com/foaf/0.1/gender) (dp)<br />[cmtq:cq_id](cq_id) (dp)<br />[foaf:givenName](http://xmlns.com/foaf/0.1/givenName) (dp)<br />[crm:P131_is_identified_by](http://www.cidoc-crm.org/cidoc-crm/P131_is_identified_by) (op)<br />[foaf:name](http://xmlns.com/foaf/0.1/name) (dp)<br />[foaf:familyName](http://xmlns.com/foaf/0.1/familyName) (dp)<br />
### E35_Title
Property | Value
--- | ---
URI | `http://www.cidoc-crm.org/cidoc-crm/E35_Title`
Description | <p>Title</p>
In domain of |[rdfs:label](http://www.w3.org/2000/01/rdf-schema#label) (dp)<br />
In range of |[crm:P102_has_title](http://www.cidoc-crm.org/cidoc-crm/P102_has_title) (op)<br />
### E41_Appellation
Property | Value
--- | ---
URI | `http://www.cidoc-crm.org/cidoc-crm/E41_Appellation`
Description | <p>Appelation</p>
In range of |[crm:P131_is_identified_by](http://www.cidoc-crm.org/cidoc-crm/P131_is_identified_by) (op)<br />
### E52_Time-Span
Property | Value
--- | ---
URI | `http://www.cidoc-crm.org/cidoc-crm/E52_Time-Span`
Description | <p>Time-Span</p>
In domain of |[crm:P82_at_some_time_within](http://www.cidoc-crm.org/cidoc-crm/P82_at_some_time_within) (op)<br />
In range of |[crm:P4_has_time-span](http://www.cidoc-crm.org/cidoc-crm/P4_has_time-span) (op)<br />
### E53_Place
Property | Value
--- | ---
URI | `http://www.cidoc-crm.org/cidoc-crm/E53_Place`
Description | <p>Place</p>
Sub-classes |[dbo:Country](http://dbpedia.org/ontology/Country) (c)<br />
In range of |[crm:P7_took_place_at](http://www.cidoc-crm.org/cidoc-crm/P7_took_place_at) (op)<br />
### E55_Type
Property | Value
--- | ---
URI | `http://www.cidoc-crm.org/cidoc-crm/E55_Type`
Description | <p>Type</p>
Sub-classes |[cmtq:RecordingWorkType](RecordingWorkType) (c)<br />[cmtq:Function](Function) (c)<br />
### E61_Time_Primitive
Property | Value
--- | ---
URI | `http://www.cidoc-crm.org/cidoc-crm/E61_Time_Primitive`
Description | 
In domain of |[cmtq:end_date](end_date) (dp)<br />[cmtq:begin_date](begin_date) (dp)<br />
In range of |[crm:P82_at_some_time_within](http://www.cidoc-crm.org/cidoc-crm/P82_at_some_time_within) (op)<br />
### E7_Activity
Property | Value
--- | ---
URI | `http://www.cidoc-crm.org/cidoc-crm/E7_Activity`
Description | <p>Activity</p>
Sub-classes |[cmtq:ProductionRoleActivity](ProductionRoleActivity) (c)<br />[cmtq:PublicWorkRelease](PublicWorkRelease) (c)<br />

## Object Properties
[country_realisation](#country_realisation),
[executed_by](#executed_by),
[has_function](#has_function),
[participated_in](#participated_in),
[realises](#realises),
[R22_created_a_realization_of](#R22_created_a_realization_of),
[R2_is_derivative_of](#R2_is_derivative_of),
[P102_has_title](#P102_has_title),
[P131_is_identified_by](#P131_is_identified_by),
[P20_had_specific_purpose](#P20_had_specific_purpose),
[P2_has_type](#P2_has_type),
[P4_has_time-span](#P4_has_time-span),
[P67_refers_to](#P67_refers_to),
[P7_took_place_at](#P7_took_place_at),
[P82_at_some_time_within](#P82_at_some_time_within),
[P87_is_identified_by](#P87_is_identified_by),
[](country_realisation)
### country_realisation
Property | Value
--- | ---
URI | `http://data.cinematheque.qc.ca/cmtq#country_realisation`
Super-properties |[crm:P7_took_place_at](http://www.cidoc-crm.org/cidoc-crm/P7_took_place_at) (op)<br />
Domain(s) |[cmtq:Production_Event](Production_Event) (c)<br />
Range(s) |[dbo:Country](http://dbpedia.org/ontology/Country) (c)<br />
[](executed_by)
### executed_by
Property | Value
--- | ---
URI | `http://data.cinematheque.qc.ca/cmtq#executed_by`
Domain(s) |[cmtq:ProductionRoleActivity](ProductionRoleActivity) (c)<br />
[](has_function)
### has_function
Property | Value
--- | ---
URI | `http://data.cinematheque.qc.ca/cmtq#has_function`
Super-properties |[crm:P2_has_type](http://www.cidoc-crm.org/cidoc-crm/P2_has_type) (op)<br />
Domain(s) |[cmtq:ProductionRoleActivity](ProductionRoleActivity) (c)<br />
Range(s) |[cmtq:Function](Function) (c)<br />
[](participated_in)
### participated_in
Property | Value
--- | ---
URI | `http://data.cinematheque.qc.ca/cmtq#participated_in`
Super-properties |[crm:P20_had_specific_purpose](http://www.cidoc-crm.org/cidoc-crm/P20_had_specific_purpose) (op)<br />
Domain(s) |[cmtq:ProductionRoleActivity](ProductionRoleActivity) (c)<br />
Range(s) |[cmtq:Production_Event](Production_Event) (c)<br />
[](realises)
### realises
Property | Value
--- | ---
URI | `http://data.cinematheque.qc.ca/cmtq#realises`
Super-properties |[frbroo:R22_created_a_realization_of](http://iflastandards.info/ns/fr/frbr/frbroo/R22_created_a_realization_of) (op)<br />
Domain(s) |[cmtq:Production_Event](Production_Event) (c)<br />
Range(s) |[cmtq:RecordingWork](RecordingWork) (c)<br />
[](R22_created_a_realization_of)
### R22_created_a_realization_of
Property | Value
--- | ---
URI | `http://iflastandards.info/ns/fr/frbr/frbroo/R22_created_a_realization_of`
Description | created a realisation of
[](R2_is_derivative_of)
### R2_is_derivative_of
Property | Value
--- | ---
URI | `http://iflastandards.info/ns/fr/frbr/frbroo/R2_is_derivative_of`
Domain(s) |[cmtq:RecordingWork](RecordingWork) (c)<br />
Range(s) |[frbroo:F1_Work](http://iflastandards.info/ns/fr/frbr/frbroo/F1_Work) (c)<br />
[](P102_has_title)
### P102_has_title
Property | Value
--- | ---
URI | `http://www.cidoc-crm.org/cidoc-crm/P102_has_title`
Description | has title
Domain(s) |[cmtq:RecordingWork](RecordingWork) (c)<br />
Range(s) |[crm:E35_Title](http://www.cidoc-crm.org/cidoc-crm/E35_Title) (c)<br />
[](P131_is_identified_by)
### P131_is_identified_by
Property | Value
--- | ---
URI | `http://www.cidoc-crm.org/cidoc-crm/P131_is_identified_by`
Description | is identified by
Domain(s) |[crm:E21_Person](http://www.cidoc-crm.org/cidoc-crm/E21_Person) (c)<br />
Range(s) |[crm:E41_Appellation](http://www.cidoc-crm.org/cidoc-crm/E41_Appellation) (c)<br />
[](P20_had_specific_purpose)
### P20_had_specific_purpose
Property | Value
--- | ---
URI | `http://www.cidoc-crm.org/cidoc-crm/P20_had_specific_purpose`
Description | had specific puropose
[](P2_has_type)
### P2_has_type
Property | Value
--- | ---
URI | `http://www.cidoc-crm.org/cidoc-crm/P2_has_type`
Description | has type
Domain(s) |[cmtq:RecordingWork](RecordingWork) (c)<br />
Range(s) |[cmtq:RecordingWorkType](RecordingWorkType) (c)<br />
[](P4_has_time-span)
### P4_has_time-span
Property | Value
--- | ---
URI | `http://www.cidoc-crm.org/cidoc-crm/P4_has_time-span`
Description | has time-span
Domain(s) |[cmtq:PublicWorkRelease](PublicWorkRelease) (c)<br />[cmtq:Production_Event](Production_Event) (c)<br />
Range(s) |[crm:E52_Time-Span](http://www.cidoc-crm.org/cidoc-crm/E52_Time-Span) (c)<br />
[](P67_refers_to)
### P67_refers_to
Property | Value
--- | ---
URI | `http://www.cidoc-crm.org/cidoc-crm/P67_refers_to`
Description | P67 is referred to by
Domain(s) |[cmtq:RecordingWork](RecordingWork) (c)<br />
Range(s) |[cmtq:PublicWorkRelease](PublicWorkRelease) (c)<br />
[](P7_took_place_at)
### P7_took_place_at
Property | Value
--- | ---
URI | `http://www.cidoc-crm.org/cidoc-crm/P7_took_place_at`
Description | P7 took place at
Domain(s) |[cmtq:PublicWorkRelease](PublicWorkRelease) (c)<br />
Range(s) |[crm:E53_Place](http://www.cidoc-crm.org/cidoc-crm/E53_Place) (c)<br />
[](P82_at_some_time_within)
### P82_at_some_time_within
Property | Value
--- | ---
URI | `http://www.cidoc-crm.org/cidoc-crm/P82_at_some_time_within`
Description | is identified by (identifies)
Domain(s) |[crm:E52_Time-Span](http://www.cidoc-crm.org/cidoc-crm/E52_Time-Span) (c)<br />
Range(s) |[crm:E61_Time_Primitive](http://www.cidoc-crm.org/cidoc-crm/E61_Time_Primitive) (c)<br />
[](P87_is_identified_by)
### P87_is_identified_by
Property | Value
--- | ---
URI | `http://www.cidoc-crm.org/cidoc-crm/P87_is_identified_by`
Description | is identified by
Domain(s) |[dbo:Country](http://dbpedia.org/ontology/Country) (c)<br />
Range(s) |[cmtq:CountryAppellation](CountryAppellation) (c)<br />

## Datatype Properties
[begin_date](#begin_date),
[cq_id](#cq_id),
[end_date](#end_date),
[label](#label),
[familyName](#familyName),
[gender](#gender),
[givenName](#givenName),
[name](#name),
[](begin_date)
### begin_date
Property | Value
--- | ---
URI | `http://data.cinematheque.qc.ca/cmtq#begin_date`
Domain(s) |[crm:E61_Time_Primitive](http://www.cidoc-crm.org/cidoc-crm/E61_Time_Primitive) (c)<br />
Range(s) |[xsd:dateTime](http://www.w3.org/2001/XMLSchema#dateTime) (c)<br />
[](cq_id)
### cq_id
Property | Value
--- | ---
URI | `http://data.cinematheque.qc.ca/cmtq#cq_id`
Domain(s) |[cmtq:RecordingWork](RecordingWork) (c)<br />[crm:E21_Person](http://www.cidoc-crm.org/cidoc-crm/E21_Person) (c)<br />[cmtq:Function](Function) (c)<br />
Range(s) |[xsd:string](http://www.w3.org/2001/XMLSchema#string) (c)<br />[xsd:positiveInteger](http://www.w3.org/2001/XMLSchema#positiveInteger) (c)<br />
[](end_date)
### end_date
Property | Value
--- | ---
URI | `http://data.cinematheque.qc.ca/cmtq#end_date`
Domain(s) |[crm:E61_Time_Primitive](http://www.cidoc-crm.org/cidoc-crm/E61_Time_Primitive) (c)<br />
Range(s) |[xsd:dateTime](http://www.w3.org/2001/XMLSchema#dateTime) (c)<br />
[](label)
### label
Property | Value
--- | ---
URI | `http://www.w3.org/2000/01/rdf-schema#label`
Domain(s) |[cmtq:RecordingWorkType](RecordingWorkType) (c)<br />[crm:E35_Title](http://www.cidoc-crm.org/cidoc-crm/E35_Title) (c)<br />[cmtq:Function](Function) (c)<br />
Range(s) |[xsd:string](http://www.w3.org/2001/XMLSchema#string) (c)<br />
[](familyName)
### familyName
Property | Value
--- | ---
URI | `http://xmlns.com/foaf/0.1/familyName`
Domain(s) |[crm:E21_Person](http://www.cidoc-crm.org/cidoc-crm/E21_Person) (c)<br />
Range(s) |[xsd:string](http://www.w3.org/2001/XMLSchema#string) (c)<br />
[](gender)
### gender
Property | Value
--- | ---
URI | `http://xmlns.com/foaf/0.1/gender`
Domain(s) |[crm:E21_Person](http://www.cidoc-crm.org/cidoc-crm/E21_Person) (c)<br />
Range(s) |[xsd:string](http://www.w3.org/2001/XMLSchema#string) (c)<br />
[](givenName)
### givenName
Property | Value
--- | ---
URI | `http://xmlns.com/foaf/0.1/givenName`
Domain(s) |[crm:E21_Person](http://www.cidoc-crm.org/cidoc-crm/E21_Person) (c)<br />
Range(s) |[xsd:string](http://www.w3.org/2001/XMLSchema#string) (c)<br />
[](name)
### name
Property | Value
--- | ---
URI | `http://xmlns.com/foaf/0.1/name`
Domain(s) |[crm:E21_Person](http://www.cidoc-crm.org/cidoc-crm/E21_Person) (c)<br />
Range(s) |[xsd:string](http://www.w3.org/2001/XMLSchema#string) (c)<br />

## Named Individuals
## Namespaces
* **cmtq**
  * `http://data.cinematheque.qc.ca/cmtq#`
* **crm**
  * `http://www.cidoc-crm.org/cidoc-crm/`
* **dbo**
  * `http://dbpedia.org/ontology/`
* **foaf**
  * `http://xmlns.com/foaf/0.1/`
* **frbroo**
  * `http://iflastandards.info/ns/fr/frbr/frbroo/`
* **owl**
  * `http://www.w3.org/2002/07/owl#`
* **prov**
  * `http://www.w3.org/ns/prov#`
* **rdf**
  * `http://www.w3.org/1999/02/22-rdf-syntax-ns#`
* **rdfs**
  * `http://www.w3.org/2000/01/rdf-schema#`
* **sdo**
  * `https://schema.org/`
* **skos**
  * `http://www.w3.org/2004/02/skos/core#`
* **xsd**
  * `http://www.w3.org/2001/XMLSchema#`

## Legend
* Classes: c
* Object Properties: op
* Functional Properties: fp
* Data Properties: dp
* Annotation Properties: dp
* Properties: p
* Named Individuals: ni