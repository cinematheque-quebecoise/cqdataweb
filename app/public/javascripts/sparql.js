// This file is part of cqdataweb.

// cqdataweb is free software: you can redistribute it and/or modify
// it under the terms of the Affero GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// cqdataweb is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// Affero GNU General Public License for more details.

// You should have received a copy of the Affero GNU General Public License
// along with cqdataweb.  If not, see <https://www.gnu.org/licenses/>.

$(() => {
  const baseUri = window.baseUri || ''
  const resourceBaseUri = window.resourceBaseUri || ''
  const exampleQueries = window.exampleQueries || []

  const defaultQuery = `SELECT * WHERE {
    ?sub ?pred ?obj .
  } LIMIT 10`

  const sparqlEndpoint = `${baseUri}/sparql`

  const sparqlTextarea = document.getElementById('sparql-query-textarea')
  const options = {
    requestConfig: {
      endpoint: sparqlEndpoint,
      method: 'GET',
      headers: { Accept: "application/sparql-results+json" },
    },
    copyEndpointOnNewTab: false,
    autofocus: true,
    endpointCatalogueOptions: {
      getData: () => {
        return [
          {
            endpoint: sparqlEndpoint
          }
          // {
          //   endpoint: "https://query.wikidata.org/sparql"
          // }
        ]
      }
    }
  }
  const yasgui = new Yasgui(sparqlTextarea, options)

  const querySelectionInput = document.querySelector('.ui.selection.dropdown input')
  querySelectionInput.addEventListener('change', e => {
    const defaultTab = yasgui.getTab()
    const queryMaybe = exampleQueries[e.target.value].query
    if (queryMaybe === undefined) {
      defaultTab.setQuery(defaultQuery)
    } else {
      defaultTab.setQuery(queryMaybe)
    }
  })
})
