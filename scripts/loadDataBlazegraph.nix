{ pkgs }:
let
  loadRestApi = import ./loadRestApi.nix { inherit pkgs; };
  waitserver = import ./waitserver.nix { inherit pkgs; };
in pkgs.runCommand "loadDataBlazegraph" {
  buildInputs = with pkgs; [ makeWrapper jdk8 curl jq loadRestApi waitserver ];
} ''
  mkdir -p $out/bin
  cp ${./loadDataBlazegraph} $out/bin/loadDataBlazegraph
  sed -i "2 i export PATH=$PATH" $out/bin/loadDataBlazegraph
  wrapProgram "$out/bin/loadDataBlazegraph" \
      --prefix PATH : $PATH
''
