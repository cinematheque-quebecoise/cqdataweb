{ pkgs }:
pkgs.runCommand "waitserver" {
    buildInputs = with pkgs; [ makeWrapper curl ];
} ''
  mkdir -p $out/bin
  cp ${./waitserver} $out/bin/waitserver
  wrapProgram "$out/bin/waitserver" \
      --prefix PATH : $PATH
''

