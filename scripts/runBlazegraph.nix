{ pkgs }:
pkgs.runCommand "runBlazegraph" {
    buildInputs = with pkgs; [ makeWrapper jdk8 ];
} ''
  mkdir -p $out/bin
  cp ${./runBlazegraph} $out/bin/runBlazegraph
  sed -i "2 i export PATH=$PATH" $out/bin/runBlazegraph
  wrapProgram "$out/bin/runBlazegraph" \
      --prefix PATH : $PATH
''
