{ pkgs }:
pkgs.runCommand "loadRestApi" {
    buildInputs = with pkgs; [ makeWrapper curl ];
} ''
  mkdir -p $out/bin
  cp ${./loadRestApi} $out/bin/loadRestApi
  wrapProgram "$out/bin/loadRestApi" \
      --prefix PATH : $PATH
''
