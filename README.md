[![pipeline status](https://gitlab.com/cinematheque-quebecoise/cqdataweb/badges/master/pipeline.svg)](https://gitlab.com/cinematheque-quebecoise/cqdataweb/-/commits/master)
[![License: AGPL v3](https://img.shields.io/badge/License-AGPLv3-blue.svg)](https://www.gnu.org/licenses/agpl-3.0.fr.html)
[![built with nix](https://builtwithnix.org/badge.svg)](https://builtwithnix.org)

# cqdataweb

Application web [data.cinematheque.qc.ca](http://data.cinematheque.qc.ca) sur les données de la Cinémathèque québécoise.

Pour utiliser ce projet, vous devez installer [Git](https://git-scm.com/downloads) et [Nix](https://nixos.org/nix/download.html) sur votre ordinateur.

L'application est testé sur le système d'exploitation GNU/Linux.

## Développement

```
# Récupèrez le projet via HTTPS
$ git clone git@gitlab.com:cinematheque-quebecoise/cqdataweb.git
# ou via SSH
$ git clone https://gitlab.com/cinematheque-quebecoise/cqdataweb.git

# Déplacez-vous dans le projet
$ cd cqdataweb

# Entrez dans un environnement isolé avec Nix
$ nix-shell

# Lancez le site web en mode développement
$ make run-dev
```

Le site web est maintenant disponible à l'adresse http://localhost:8080

## Déploiement

L'application peut être déployée de plusieurs façons. Nous présentons le déploiement local avec VirtualBox et le déploiment en production sur un serveur de [DigitalOcean](https://cloud.digitalocean.com). Nous utilisons [nixops](https://nixos.org/nixops/) pour effectuer le déploiement.

### VirtualBox

Assurez-vous d'installer `VirtualBox` sur votre ordinateur.

Pour plus d'informations sur le déploiement sur VirtualBox, lisez la documentation suivante: https://releases.nixos.org/nixops/latest/manual/manual.html#idm140737322662048

Tout d'abord, créez la configuration de déploiement initial:

```
$ make vbox-init
```

Ensuite, déployez l'application sur `VirtualBox`.

```
$ make vbox-deploy
```

Le déploiement peut durer un long moment étant donné qu'on initialise la base de données Blazegraph avec les données RDF.

Si vous modifiez l'application et souhaitez redéployer, relancez `make vbox-deploy`.

Pour supprimer la machine virtelle:

```
$ make vbox-destroy
```

### DigitalOcean

Pour plus d'informations sur le déploiement sur DigitalOcean, lisez la documentation suivante: https://releases.nixos.org/nixops/latest/manual/manual.html#sec-deploying-to-digital-ocean

Assurez-vous d'avoir mis en place une clé SSH entre votre ordinateur et DigitalOcean.

De plus, vous devez générer un jeton d'accès vers l'API de DigitalOcean en allant sur l'onglet "API" de DigitalOcean et le copier/coller ci-bas:
```
$ export DIGITAL_OCEAN_AUTH_TOKEN=<VOTRE_JETON_DIGITAL_OCEAN> # Ex. export DIGITAL_OCEAN_AUTH_TOKEN=f72b3c1cbf944affb4927fe32e3e782c
```

Actuellement, le site web data.cinematheque.qc.ca est déployé sur DigitalOcean. Voici les étapes pour effectuer un redéploiement avec de nouvelles modifications:

1. Récupérez le fichier `cqdataweb.json` et modifiez les chemins de l'attribut `nixExprs` pour pointer vers le bonne endroit selon votre dossier.
2. Importer localement la configuration de déploiement avec `make digitalocean-import`
3. Redéployez avec `make digitalocean-deploy`

Si vous modifiez le code source du site web, simplement relancer `make digitalocean-deploy` pour redéployer.

#### Création d'un nouveau droplet

Si vous désirez effectuer un déploiement sur une nouvelle machine (droplet) de DigitalOcean, alors exécutez les commandes suivantes:

```
# Créez la configuration de déploiement initial (besoin d'être lancé UNE SEULE FOIS)
$ make digitalocean-init

# Déployez l'application sur DigitalOcean
$ make digitalocean-deploy # Créé une nouvelle machine (droplet) sur DigitalOcean
```

Un droplet sera créé sur DigitalOcean qui contiendra votre site web déployé. Si vous modifiez le code source du site web, simplement relancer `make digitalocean-deploy` pour redéployer.

#### Sauvegarde de la configuration de NixOps

Après avoir créé le droplet avec NixOps, vous devez sauvegarder la configuration avec un fichier JSON afin que d'autres utilisateurs puissent redéployer sur ce même droplet à partir d'un autre ordinateur afin de garder la même adresse IP.

```
# À partir de l'ordinateur où vous avez fait le 'make digitalocean-init'
$ make digitalocean-export
```

Sauvegardez le fichier JSON dans un endroit sécuritaire et privé, car il contient des informations confidentielles.

## License

`cqdataweb` est licensié sous la license [AGPLv3](https://opensource.org/licenses/AGPL-3.0). Voir le fichier [LICENSE](./LICENSE) pour obtenir le texte complet de la license.
