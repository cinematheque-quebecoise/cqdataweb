let

sources = import ./nix/sources.nix {};
overlay = _: pkgs:
      { niv = (import sources.niv {}).niv;    # use the sources :)
      };
pkgs = import sources.nixpkgs                  # and use them again!
  { overlays = [ overlay ] ; config = {}; };

waitserver = import ./scripts/waitserver.nix { inherit pkgs; };
loadDataBlazegraph = import ./scripts/loadDataBlazegraph.nix { inherit pkgs; };
runBlazegraph = import ./scripts/runBlazegraph.nix { inherit pkgs; };

in
pkgs.mkShell {
  buildInputs = [
    pkgs.nixops
    pkgs.curl
    pkgs.zip
    pkgs.nodejs-10_x
    pkgs.nodePackages.node2nix
    pkgs.nginx
    pkgs.openjdk

    waitserver
    loadDataBlazegraph
    runBlazegraph
  ];

  BLAZEGRAPH_JAR_FPATH = import ./nix/blazegraph.nix { inherit pkgs; };

  APPAREILS_IMAGES_PATH = import ./nix/appareils.nix { inherit pkgs; };
}
